'use strict';

/**
 * Retrieves source from multiple repositories and copies to the output directory.  Source repositories are put in exports
 * file:// repositories are copies directly to output directory.
 *
 * @Contributors: Holger Nestmann, Danny Gehl, Jason Moody, Danny Domhardt
 * @DocRef: https://github.com/gruntjs/grunt-contrib-copy
 **/

var path = require('path');
var getMoveParameters = require('./util/get_move_parameters');
var runEncryption = require('./util/encryption.js');

/**
 * Builds 
 */
function configureOptimize(moveParameters, grunt) {
	var version = grunt.config.get('version');
	moveParameters.options = {
		//Only process ISML files ("!**/*.isml" does not work here)
		noProcess: ["**/static/**", "**/pipelines/**", "**/scripts/**", "**/webreferences*/**"],
		process: function (content, srcpath) {
			if(srcpath.substring(srcpath.length - 5).toLowerCase() !== '.isml') {
				return content;
			}
			
			//Base Path is the CWD
			var basePath = moveParameters.files[0].cwd.replace(/\/\./g, '') + '/';
			
			//Cartridge name is the folder under the CWD in the source path (= path to ISML)
			var cartridgeName = srcpath.substring(basePath.length).split('/')[0];

			//Merge JS files and replace the tagged sections
			if (grunt.config('settings.build\\.optimize\\.js')) {
				content = content.replace(/<!--- BEGIN JS files to merge(.*)--->([\s\S]*)\s*<!--- END JS files to merge(.*)--->/g, function (all, params, scripts) {
					var relpath = params.match(/source_path=([^;)]*)/)[1];
					var target = params.match(/targetfile=([^;)]*)/)[1];

					var sourceFiles = scripts.replace(/.*\('/g, basePath + cartridgeName + '/' + relpath ).replace(/'\).*/g, '').match(/[^\r\n]+/g).filter(function (e) {
						return e.length && e.indexOf('.js') > -1;
					});
					
					var targetFile = 'output/code/' + version + '/' + cartridgeName + '/' + relpath + '/' + target;
					grunt.log.writeln("Generating task 'uglify."+(target.split('.')[0])+"' to optimize "+JSON.stringify(sourceFiles, null, 1)+" into "+targetFile);
					
					// create uglify config
					grunt.config('uglify.' + target.split('.')[0], {
						src: sourceFiles,
						dest: targetFile
					});
					return '<script type="text/javascript" src="${URLUtils.absStatic(\'' + target + '\')}"></script>';
				});
			}

			//Merge CSS files and replace the tagged sections
			if (grunt.config('settings.build\\.optimize\\.css')) {
				content = content.replace(/<!--- BEGIN CSS files to merge(.*)--->([\s\S]*)\s*<!--- END CSS files to merge(.*)--->/g, function (all, params, scripts) {
					var relpath = params.match(/source_path=([^;)]*)/)[1];
					var target = params.match(/targetfile=([^;)]*)/)[1];

					var sourceFiles = scripts.replace(/.*\('/g, basePath + cartridgeName + '/' + relpath).replace(/'\).*/g, '').match(/[^\r\n]+/g).filter(function (e) {
						return e.length && e.indexOf('.css') > -1;
					});
					
					var targetFile = 'output/code/' + version + '/' + cartridgeName + '/' + relpath + '/' + target;
					grunt.log.writeln("Generating task 'cssmin."+(target.split('.')[0])+"' to optimize "+JSON.stringify(sourceFiles, null, 1)+" into "+targetFile);

					// create cssmin config
					grunt.config('cssmin.' + target.split('.')[0], {
						src: sourceFiles,
						dest: targetFile
					});
					
					return '<link href="${URLUtils.absStatic(\'' + target + '\')}" rel="stylesheet" type="text/css" />';
				});
			}
			return content;
		}
	};
}


function configureGITCheckout(cloneOptions, grunt) {
	var checkoutpath = 'exports/' + cloneOptions.id;

	if (grunt.file.exists(checkoutpath)) {
		grunt.config('gitfetch.' + cloneOptions.id, {
			options : {
				cwd: checkoutpath,
				all: true
			}
		});
		grunt.config('gitreset.' + cloneOptions.id, {
			options : {
				cwd: checkoutpath,
				mode: 'hard'
			}
		});

		return ['gitfetch:' + cloneOptions.id, 'gitreset:' + cloneOptions.id];
	}
	else {
		var url = cloneOptions.repository.url; 
		
		//Possibilty to provide username and password in config file
		if(cloneOptions.repository.username && cloneOptions.repository.password) {
			if(url.indexOf('https://') !== 0) {
				grunt.fail.warn('Providing username and password for git repositories only works for HTTPS urls. Please fix.');
			}
			
			var tmpUrl = url.replace('https://', '');
			var url = 'https://' + cloneOptions.repository.username + ':' +  cloneOptions.repository.password + '@' + tmpUrl;
		}
		
		grunt.config('gitclone.' + cloneOptions.id, {
			options : {
				repository: url,
				branch: (cloneOptions.repository.branch || 'master'),
				directory: checkoutpath
			}
		});
		return ['gitclone:' + cloneOptions.id];
	}
}

function configureSVNCheckout(cloneOptions, grunt) {
	grunt.config('dw_svncheckout.' + cloneOptions.id + '.options', cloneOptions);

	return ['dw_svncheckout:' + cloneOptions.id];
}


/*
 * Set up site import based on configuration. 
 * 
 * Constists of 3 parts now: initialization, demo data and target instance based replacements
 * Everything is copied to output folder, the import task decides which of the parts are actually imported.
 */
function configureSiteImport(grunt, instance, cloneOptions) {
	//Global site import path with fallback to old config structure
	var siteInitPath = instance['site_import.path'] || 'sites/site_template';
	
	//Demo + Instance default paths
	var siteDemoPath = 'sites/site_demo';
	var instancePath = 'sites/config';
	
	//Check for Repository-based site import path (new config structure)
	if(cloneOptions.siteImport) {
		siteInitPath = cloneOptions.siteImport.initPath || siteInitPath;
		siteDemoPath = cloneOptions.siteImport.demoPath || siteDemoPath;
		instancePath = cloneOptions.siteImport.instancePath || instancePath;
	}
	
	var initSourceDir = 'exports/' + cloneOptions.id + '/' + siteInitPath;
	var demoSourceDir = 'exports/' + cloneOptions.id + '/' + siteDemoPath;
	var instanceSourceDir = 'exports/' + cloneOptions.id + '/' + instancePath;

	//Handle local repository
	if(cloneOptions.repository.type === 'file') {
		initSourceDir = path.normalize(cloneOptions.repository.url.slice('file://'.length) + '/' + siteInitPath);
		demoSourceDir = path.normalize(cloneOptions.repository.url.slice('file://'.length) + '/' + siteDemoPath);
		instanceSourceDir = path.normalize(cloneOptions.repository.url.slice('file://'.length) + '/' + instancePath);
	}

	var siteTemplateMoveParameters = getMoveParameters({
		grunt: grunt,
		includeDirs: ['.'],
		cwd: initSourceDir,
		dest: 'output/site_import/site_init',
		sourceGlob: '/**/*'
	});
	
	var siteDemoMoveParameters = getMoveParameters({
		grunt: grunt,
		includeDirs: ['.'],
		cwd: demoSourceDir,
		dest: 'output/site_import/site_demo',
		sourceGlob: '/**/*'
	}); 

	var instanceMoveParameters = getMoveParameters({
		grunt: grunt,
		includeDirs: ['.'],
		cwd: instanceSourceDir,
		dest: 'output/site_import/config',
		sourceGlob: '/**/*'
	});
	
	grunt.config('copy.site_init_' + cloneOptions.id, siteTemplateMoveParameters);
	grunt.config('copy.site_demo_' + cloneOptions.id, siteDemoMoveParameters);
	grunt.config('copy.site_config_' + cloneOptions.id, instanceMoveParameters);
}



/**
 * outEmptyString
 *
 * @param value
 * @return
 */
function outEmptyString(value) {
	return value !== '';
}

/**
 * Converts either a string or object in the dependencies array into a
 * clone options object used by the clone task.
 *
 * @param value
 * @return
 */
function toCloneOptions(value) {
	
	var cloneOptionsDefaults = {
		includeCartridges: [],
		// Keep `null` so that each VCS type can determine the default.
		branch: null,
		directory: 'exports/',
		type: null
	};

	var cloneOptions = null;

	// Allow a string for backwards compatibility.
	if (typeof value === 'string') {
		cloneOptions = Object.create(cloneOptionsDefaults);

		var options = value.split(/ -\s?/);
		cloneOptions.repository.url = options[0];

		var includeCartridges = cloneOptions.includeCartridges;

		options.forEach(function (option) {
			var parts = option.split(' ');
			var name = parts[0];
			var value = parts[1];

			if (name === 'include-cartridges') {
				var dirs = value.split(',');

				// Add all dirs into the `includeDirs` array.
				includeCartridges.push.apply(includeCartridges, dirs);
			}
			else {
				cloneOptions[name] = value;
			}
		});
	}
	else {
		cloneOptions = value;
		Object.setPrototypeOf(cloneOptions, cloneOptionsDefaults);
	}
	
	//Stay compatible to old configuration style
	if(!cloneOptions.repository.url) {
		var repo = cloneOptions.repository || '';
		var type = cloneOptions.type;
		var branch = cloneOptions.branch;
		
		cloneOptions.repository = {
				url: repo,
				type: type,
				branch: branch
		};		
	}

	// Augment the defaults to allow an implicit id.
	var url = cloneOptions.repository.url;
	
	//Remove trailing slashes as it will break task id generation
	if(url.lastIndexOf('/') === url.length - 1) {
		url = url.substring(0, url.length -1);
	}
	
	var isSSHRepo = url.indexOf('/') === -1;
	cloneOptionsDefaults.id = url.slice(( isSSHRepo ? url.lastIndexOf(':'): url.lastIndexOf('/')) + 1);

	// Try and detect the type if it wasn't explicity passed.
	if (!cloneOptions.repository.type) {
		// Is this a Git repository?
		if (url.indexOf('git') > -1) {
			cloneOptions.repository.type = 'git';
		}
		// What about an SVN repository?
		else if (url.indexOf('svn') > -1) {
			cloneOptions.repository.type = 'svn';
		}
		else {
			cloneOptions.repository.type = 'file';
		}
	}
	
	//Check for relative local paths and resolve them
	if(cloneOptions.repository.type === 'file' && url.indexOf('file:') !== 0 && url.indexOf('/') !== 0 && url.indexOf('\\') !== 0) {
		url = 'file://' + path.resolve(url);
		url = url.replace(/\\/g,'/');
		
		cloneOptions.repository.url = url;
		cloneOptionsDefaults.id = url.slice(( url.lastIndexOf('/')) + 1);
	}
	
	//Clean ".git" off the end of the repository name in case of Git Repo
	if (cloneOptions.repository.type === 'git') {
		cloneOptionsDefaults.id = cloneOptionsDefaults.id.replace('.git', '');
	}
	
	//Dots will screw up the task reference so we replace all of them
	cloneOptionsDefaults.id = cloneOptionsDefaults.id.replace(/\./g, '_');

	return cloneOptions;
}


function buildVersion(grunt) {
	var project_name = grunt.option('build.project.name') || grunt.config('settings.build\\.project\\.name'); 
	grunt.log.writeln("Building project: " + project_name);
	
	var version_name = grunt.option('build.project.version') || grunt.config('settings.build\\.project\\.version');
	grunt.config('settings.build\\.project\\.version', version_name);

	var build_number = grunt.option('build.project.number') || grunt.config('settings.build\\.project\\.number');
	if (build_number) {
		version_name += '-' + build_number;
	}

	grunt.config('version', version_name);
	grunt.log.writeln("Build version: " + version_name);

	return version_name;
}


function buildHTTPConfig(instance, cartridgeArchiveName, method) {
	return {
		options: {
			auth: {
				user: instance["webdav.username"],
				pass: instance["webdav.password"]
			},
			url: 'https://' + instance["webdav.server"] + instance["webdav.cartridge.root"] + cartridgeArchiveName,
			form: {
				method: method
			}
		}
	};
}


module.exports = function (grunt) {
	runEncryption(grunt);
	
	var settings = grunt.config.get('settings');
	var instance = grunt.config.get('instance');

	var version = buildVersion(grunt);

	// Try and intelligently decide which configuration file to load.
	var configLines = grunt.config('dependency');

	//Global source path setting is considered deprecated
	var sourcePath = instance['source.path'] || 'cartridges';
	
	if(instance['source.path']) {
		grunt.log.warn('Use of global source path is discouraged - please use repository-based source:path option.');
	}

	//Global source glob setting is considered deprecated
	var sourceGlob = instance['source.glob'] ? instance['source.glob'].split(',') : '**/*';
	
	if(instance['source.glob']) {
		grunt.log.warn('Use of global source globbing is discouraged - please use repository-based source:glob option.');
	}

	var checkoutTasks = [];
	var compressTasks = [];
	var uploadTasks = [];
	var unzipTasks = [];
	var deleteTasks = [];

	configLines
		.filter(outEmptyString)
		.map(toCloneOptions)
		.forEach(function (cloneOptions) {
			
			//Check Repository ID
			if(cloneOptions.id.length < 2) {
				grunt.fail.warn('Task name could not be generated properly. Please check. URL: ' + cloneOptions.repository.url + ', Repository ID: ' + cloneOptions.id);
			}
			
			//Enforce URI schema for local file paths
			if(cloneOptions.repository.type === 'file' && cloneOptions.repository.url.indexOf('file:') !== 0) {
				grunt.fail.warn('Please enter absolute local paths in URI form (starting with "file://"). Repository: ' + cloneOptions.id + "\n");
			}
			
			var currentSourcePath = sourcePath;
			var currentSourceGlob = sourceGlob;
			var ignoreEmpty = false;
			
			//path + glob now configurable on repository-basis (backwards compatible), "ignoreEmpty" is used to suppress warnings
			if(cloneOptions.source) {
				currentSourcePath = cloneOptions.source.path || sourcePath;
				currentSourceGlob = cloneOptions.source.glob || sourceGlob;
				ignoreEmpty = cloneOptions.source.ignoreEmpty;
			}
			
			var cwd = 'exports/' + cloneOptions.id + '/' + currentSourcePath;

			grunt.log.writeln('');
			grunt.log.writeln('--- Repository: ' + cloneOptions.repository.url);

			if (cloneOptions.repository.type === 'file') {
				// checkout - local repository (nothing to do here except modifying cwd)
				cwd = path.normalize(cloneOptions.repository.url.slice('file://'.length) + '/' + currentSourcePath);
				
				if(!grunt.file.isDir(cwd)) {
					grunt.log.warn('Cartridge directory "' + cwd + '" does not exist. Please check your settings.');
				}
			}
			else if (cloneOptions.repository.type === 'git') {
				// checkout - GIT repository
				checkoutTasks.push.apply(checkoutTasks, configureGITCheckout(cloneOptions, grunt));
				
				grunt.log.writeln('Source path: "' + cwd + '"');
			}
			else if (cloneOptions.repository.type === 'svn') {
				// checkout -  SVN repository
				checkoutTasks.push.apply(checkoutTasks, configureSVNCheckout(cloneOptions, grunt));
				
				grunt.log.writeln('Source path: "' + cwd + '"');
			}
			
			var moveParameters = getMoveParameters({
				grunt: grunt,
				includeDirs: cloneOptions.includeCartridges,
				sourceGlob: currentSourceGlob,
				cwd: cwd,
				dest: 'output/code/' + version + '/',
				nonull: !ignoreEmpty 
			});

			if (settings['build.optimize.js'] || settings['build.optimize.css']) {
				configureOptimize(moveParameters, grunt);
			}

			grunt.config('copy.' + cloneOptions.id, moveParameters);

			if (settings['code.upload.granularity'] === 'CARTRIDGE') {
				// create upload ZIP file per cartridge
				cloneOptions.includeCartridges.forEach(function (cartridge) {

					var cartridgeArchiveName = cartridge + '-' + version + '.zip';
					var cartridgeArchivePath = 'output/code/' + cartridgeArchiveName;

					// define parameters for compress task
					grunt.config('compress.code_' + cartridge, {
						options: {
							archive: cartridgeArchivePath
						},
						files: [{
							src: ['**'],
							cwd: 'output/code/' + version + '/' + cartridge,
							dest: version + '/' + cartridge,
							expand: true
						}]
					});
					compressTasks.push('compress:code_' + cartridge);

					// define parameters for upload task
					grunt.config('dw_upload.code_' + cartridge, {
						options: {
							release_path: instance["webdav.cartridge.root"] + cartridgeArchiveName
						},
						files: {
							src: cartridgeArchivePath
						}
					});
					uploadTasks.push('dw_upload:code_' + cartridge);

					// define parameters for unzip task
					grunt.config('http.unzipUpload_' + cartridge, buildHTTPConfig(instance, cartridgeArchiveName, 'UNZIP'));
					unzipTasks.push('http:unzipUpload_' + cartridge);

					// define parameters for delete task
					if (settings['code.upload.cleanup']) {
						grunt.config('http.deleteUpload_' + cartridge, buildHTTPConfig(instance, cartridgeArchiveName, 'DELETE'));
						deleteTasks.push('http:deleteUpload_' + cartridge);
					}
				});
			}

			//Check whether site import (initialization/demodata) is triggered
			if (settings['build.project.codeonly']) {
				grunt.log.writeln('Site import for ' + cloneOptions.id + ' disabled globally (codeonly flag set).');
			}
			else if(cloneOptions.siteImport && cloneOptions.siteImport.enabled !== 'true') {
				grunt.log.writeln('Site import for  ' + cloneOptions.id + ' disabled at repository level.');
			}
			else {
				configureSiteImport(grunt, instance, cloneOptions);
			}
		});

		grunt.registerTask('checkout', checkoutTasks);

		if (!settings['code.upload.granularity'] || settings['code.upload.granularity'] === 'VERSION') {

			// define parameters for compress task
			grunt.config('compress.code',
				{
					options: {
						archive: 'output/code/' + version + '.zip'
					},
					files: [{
						src: ['**'],
						cwd: 'output/code/' + version,
						dest: version,
						expand: true
					}]
				}
			);

			// define parameters for upload task
			grunt.config('dw_upload.code', {
				options: {
					release_path: instance["webdav.cartridge.root"] + version + '.zip'
				},
				files: {
					src: 'output/code/' + version + '.zip'
				}
			});

			// define parameters for unzip task
			grunt.config('http.unzipUpload', buildHTTPConfig(instance, version + '.zip', 'UNZIP'));

			// define parameters for delete task
			if (settings['code.upload.cleanup']) {
				// define parameters for delete task
				grunt.config('http.deleteUpload', buildHTTPConfig(instance, version + '.zip', 'DELETE'));
			}
		}
		else {
			grunt.registerTask('compress:code', compressTasks);
			grunt.registerTask('dw_upload:code', uploadTasks);
			grunt.registerTask('http:unzipUpload', unzipTasks);
			grunt.registerTask('http:deleteUpload', deleteTasks);
		}

		grunt.log.verbose.writeln(JSON.stringify(grunt.config(), null, 1));
};