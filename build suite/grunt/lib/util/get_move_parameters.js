/**
 * getMoveParameters
 *
 * @param options
 * @return
 */
module.exports = function(options) {
	var moveParameters = { files:[] };
	var includeDirs = options.includeDirs;
	var cwd = options.cwd;
	var dest = options.dest;
	var sourceGlob = options.sourceGlob;
	var nonull = options.nonull;

	includeDirs.forEach(function(dir){
		moveParameters.files.push({
			expand: true,
			cwd: cwd, //repositoryPath
			src: dir + '/**/*',
			dest: dest
		});
	});

	if (moveParameters.files.length === 0) {
		moveParameters.files.push({
			expand: true,
			cwd: cwd, //repositoryPath
			src: sourceGlob,
			dest: dest,
			nonull: nonull
		});
	}

	return moveParameters;
};