'use strict';
/**
 * @module
 * Module containing functions for creating site imports
 */
module.exports = (function(grunt) {
	var importPath;

	/**
	* @private
	*/
	function getVersion() {
		return grunt.config.get('settings.config\\.archive\\.name');
	}

	/**
	 * @private
	 */
	function getImportPath() {
		if (!importPath || !grunt.file.isDir(importPath)) {
			grunt.fail.fatal('Import path is not set or is not a directory');
		}

		return importPath;
	}

	/**
	 * @private
	 */
	function copyConfiguration(dir) {
		var src = [dir + '/**/*', '!' + dir + '/*.zip', '!' + dir + '/' + getVersion() + '/**/*'];
		var dest = 'output/site_import/' + getVersion();

		var filter = grunt.config.get('instance.site_import\\.site\\.filter');
		if(filter && typeof filter.map === 'function'){
			src.push('!'+dir + '/sites/**/*');
			src = src.concat(filter.map(function(site){
				return dir + '/sites/' + site+'/**/*';
			}));
			grunt.log.writeln("Found site filter, calculated globbing pattern: "+src);
		}

		grunt.file.expand(src).forEach(function(file) {
			if (grunt.file.isFile(file)) {
				var f = file.replace(dir, '');
				grunt.file.copy(file, dest + f);
			}
		});
	}

	/**
	 *
	 * retrieves files based on replacement file values
	 * @private
	 * @param {Object} replacement - the replacement object
	 */
	function fetchFiles(replacement) {
		// use globbing to modify all files matching given pattern in given rule.
		var files = {};
		replacement.files.forEach(function(file) {
			var allFiles = grunt.file.expand(getBasePath() + '/' + file);
			grunt.log.verbose.writeln('Globbed ' + file + ' to: ', allFiles);

			allFiles.forEach(function(f) {
				files[f] = f;
			});
		});

		return files;
	}

	/**
	 *
	 * @private
	 */
	function getBasePath() {
		return 'output/site_import/' + getVersion() + '/';
	}
	
	
	/**
	 *
	 * update data in xml and text files
	 * @public
	 * @param {Object} replacements - object containing xml and text replacements
	 */
	function updateData(replacements) {
		if (replacements.xmlReplacements) {
			updateXML(replacements.xmlReplacements);
		}

		if (replacements.textReplacements) {
			updateText(replacements.textReplacements);
		}
	}
	
	
	/**
	 *
	 * Modify configuration in the output/site_import folder.
	 * Configures an xmlpoke task to be run after the current executing task.
	 * @private
	 */
	function updateXML(replacements) {
		grunt.log.verbose.writeln('updateXML rules ', replacements);

		// Setup xmlpoke configuration based on the given rules array.

		var files;

		if (typeof replacements !== 'undefined' && replacements.length > 0) {
			replacements.forEach(function(replacement, index) {
				files = fetchFiles(replacement);

				grunt.log.verbose.writeln('Set "' + replacement.xpath + '" to "' + replacement.value + (replacement.valueType ? '" (type: "' + replacement.valueType + '")' : ''), files);

				grunt.config('xmlpoke.' + index, {
					options: {
						xpath: replacement.xpath,
						value: replacement.value,
						namespaces: replacement.namespaces ? replacement.namespaces : {},
						valueType: replacement.valueType ? replacement.valueType : 'text'
					},
					files: files
				});
			});

			// run the replacements
			grunt.task.run('xmlpoke');
		}
	}


	/**
	 *
	 * update text replacement data based on grouped files
	 * @private
	 * @param {Array} replacements - Array containing text replacement data
	 */
	function updateText(replacements) {
		var basePath = getBasePath(),
			basePathSep = '|',
			srcString,
			srcFiles = [];

		replacements.forEach(function(replacement, index) {
			srcString = basePath + replacement.files.join(basePathSep + basePath);
			srcFiles = srcString.split(basePathSep);
			grunt.log.verbose.writeln('Set text replace ' + replacement.regex + ' to ' + replacement.value, ' for files', srcFiles);
			grunt.config('replace.' + (index+1), {
				src: srcFiles,
				overwrite: true,
				replacements: [{
					from: replacement.regex,
					to: replacement.value
				}]
			});

			// run the replacements
			grunt.task.run('replace');
		});

	}

	
	/**
	 * Copy generic configuration into output/site_import folder.
	 */
	function copySiteInitData() {
		copyConfiguration(getImportPath() + '/site_init');
	}

	
	/**
	 * @public Copy demo site data to site_import folder
	 */
	function copySiteDemoData() {
		copyConfiguration(getImportPath() + '/site_demo');
	}

	
	/**
	* Copy a complete configuration, no need for updating XML and copying extra files.
	 * @public
	 */
	function copyComplete() {
		copyConfiguration(getImportPath());
	}

	/**
	 * Clear given folder in output directory, by default it will clear the site_import folder.
	 * @public
	 */
	function cleanup() {
		var directory = getBasePath();
		
		if (grunt.file.isDir(directory)) {
			grunt.log.verbose.writeln('Cleanup output folder ' + directory);
			grunt.file.delete(directory);
		}
	}


	/**
 	* @public
 	*/
	function setSourceImportPath(path) {
		grunt.log.verbose.writeln('Site import path is: ', path);
		importPath = path;
	}

	
	return {
		copySiteDemoData: copySiteDemoData,
		copySiteInitData: copySiteInitData,
		copyComplete: copyComplete,
		cleanup: cleanup,
		setSourceImportPath: setSourceImportPath,
		updateData: updateData
	};
});
