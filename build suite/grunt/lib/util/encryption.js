'use strict';

var crypto = require('crypto');

module.exports = function (grunt) {
	var ENCRYPTION_MARKER = 't8kdrXdL61E_';
	var ENCRYPTION_PASSPHRASE = 'gBqZACrbEMdiICjSerRWzFeK';
		
	if (!grunt.config('settings.password\\.encryption')) {
		grunt.log.writeln('Password encryption is disabled!');
		return;
	}
	
	grunt.log.writeln('Password encryption is enanbled.');
		
	var password = grunt.config('instance.webdav\\.password');
	
	if(!password) {
		grunt.log.writeln('No password found. Nothing to encrypt.');
		return;
	}
	
	var dependencyFilename = 'build/projects/' + grunt.config('settings.build\\.project\\.name') + '/config.json';

	if(!grunt.file.exists(dependencyFilename)) {
		grunt.fail.warn('Could not find project configuration file. Skipping.');
		return;
	}
	
	if (password.indexOf(ENCRYPTION_MARKER) === 0) {
		password = password.substring(ENCRYPTION_MARKER.length);
		
		var decipher = crypto.createDecipheriv('des-ede3', ENCRYPTION_PASSPHRASE, "");
		var plain_pwd = decipher.update(password, 'base64', 'utf8');
		plain_pwd += decipher.final('utf8');
		
		grunt.config('instance.webdav\\.password', plain_pwd);
	} else {
		var cipher = crypto.createCipheriv('des-ede3', ENCRYPTION_PASSPHRASE, "");
		
		var encrypted_pwd = cipher.update(password, 'utf8', 'base64');
		encrypted_pwd += cipher.final('base64');

		if(encrypted_pwd != null)
		{
			var fileContent = grunt.file.read(dependencyFilename);
			grunt.file.write(dependencyFilename,fileContent.replace(password, ENCRYPTION_MARKER+encrypted_pwd));
			grunt.log.writeln('Password has been encrypted for security reasons.');
			grunt.config('instance.webdav\\.password',password);
		}
		else {
			grunt.fail.warn('Password encryption failed!');
		}
	}
};
