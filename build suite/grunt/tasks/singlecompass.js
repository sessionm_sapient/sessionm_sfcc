module.exports = function(grunt) {

  grunt.registerTask('singlecompass', 'compile with compass', function(cartridge,locale){
	var compassConfigPath = grunt.config.get('instance.sass.compassConfigPath');
	var version = grunt.config.get('version');
	var basePath = 'output/code/'+version+'/'+cartridge+'/cartridge/'+compassConfigPath+'/'+locale;
	var filename = basePath+'/config.rb';
	if (grunt.file.exists(filename)) {
		grunt.log.writeln('compass compile for cartridge: "' + cartridge + '"');
		grunt.option('build.options.config', filename);
		grunt.option('build.options.basePath', basePath);
		grunt.task.run('compass:build');
	}
  });

};
