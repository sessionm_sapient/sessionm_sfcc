module.exports = function(grunt) {

  grunt.registerTask('multicompass', 'compile with compass', function(){
	var compassConfigPath = grunt.config.get('instance.sass.compassConfigPath');
	var locales = grunt.config.get('instance.sass.compassConfigLocales');
	var version = grunt.config.get('version');
	var configLines = grunt.config('dependency');
	configLines.forEach(function (cloneOptions) {
		cloneOptions.includeCartridges.forEach(function (cartridge) {
			locales.forEach(function (locale) {
				var basePath = 'output/code/'+version+'/'+cartridge+'/cartridge/'+compassConfigPath+'/'+locale;
				var filename = basePath+'/config.rb';
				grunt.log.writeln('filename: '+filename);
				if (!grunt.file.exists(filename)) {
					grunt.task.run('sass:build:'+cartridge+':'+locale);
				} else {
				grunt.task.run('singlecompass:'+cartridge+':'+locale);
				}
			});
		});
	});
  });

};