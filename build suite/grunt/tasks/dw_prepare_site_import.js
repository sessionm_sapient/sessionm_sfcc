'use strict';

/**
 *	Create site import configuration structure
 **/
module.exports = function(grunt) {
	grunt.registerMultiTask('dw_prepare_site_import', 'Create site import package', function() {
		
		var	instance = grunt.config.get('instance');
		var	options = this.options();
		
		//Ensure we're compatible to older config formats
		if(!instance.siteImport) {
			var target = instance['site_import.instance'] || instance['site.import.instance'];
				
			if(target) {
				grunt.log.warn('Site Import Instance: Please switch to object notation and use siteImport:targetInstance.');
			}
				
			instance.siteImport = {
					targetInstance: target
			};
		}
		
		var siteImport = require('../lib/util/site_import')(grunt);
		siteImport.setSourceImportPath('output/site_import');
		
		siteImport.cleanup();

		//Import site initialization data if set
		if(options.importInit) {
			grunt.log.writeln("Importing Site initialization data");
			siteImport.copySiteInitData();
		} 
		
		//Import demo site if set
		if(options.importDemo) {
			grunt.log.writeln("Importing demo data for Site.");
			siteImport.copySiteDemoData();
		}
		
		//Instance-specific import and application of eventually configured replacement
		var instanceConfig = instance.siteImport ? loadInstanceConfig(instance.siteImport.targetInstance) : null;
		
		if(instanceConfig) {
			grunt.log.writeln("Applying replacements for target instance: " + instance.siteImport.targetInstance);
			siteImport.updateData(instanceConfig.replacements);
		}
	});
	
	
	/**
	 * Loads instance-based configuration data
	 * 
	 * @param instance
	 */
	function loadInstanceConfig(target)
	{
		var instanceConfig;
		
		//Load corresponding config if target instance is given
		if(target) 
		{
			var instanceConfigFile = 'output/site_import/config/' + target + '/config.json';
			grunt.log.verbose.writeln('Reading instance configuration from: ' + instanceConfigFile);
			
			if(grunt.file.exists(instanceConfigFile))
			{
				instanceConfig = grunt.file.readJSON(instanceConfigFile);
				grunt.log.verbose.writeln('Using instance configuration: ' + instanceConfig);
			}
			
			if(!instanceConfig) {
				grunt.log.warn('No config.json found for instance ' + target);
			}
		}

		return instanceConfig;
	}
};


