'use strict';

var url = require('url');
var path = require('path');
var which = require('which');

module.exports = function(grunt) {
	grunt.registerMultiTask('dw_svncheckout', 'SVN checkout task.', function() {

		var done = this.async();
		var options = this.options();

		var pathname = url.parse(options.repository.url).pathname;
		var name = pathname.slice(pathname.lastIndexOf('/') + 1);

		var out = path.join(__dirname, '../../exports', name);

		// Windows bug.  Create directory path
		grunt.file.mkdir(out);

		var checkoutUrl = "";
		
		var uid = "";
		var pwd = "";

		if (options.repository.url.indexOf("@") > 0) {
			// Extract Username Password from repository URL
			var arrUrl = options.repository.url.split("//");
			var tmpUrl = arrUrl[1] || arrUrl[0];
			
			var substrCredentials = tmpUrl.substring(0, tmpUrl.lastIndexOf('@'));
			var credentials = substrCredentials.split(":");

			checkoutUrl = "https://" + tmpUrl.substring(tmpUrl.lastIndexOf('@') + 1) + "/" + (options.repository.branch || '');
			
			uid = credentials[0];
			pwd = credentials[1];
		}
		else {
			checkoutUrl = options.repository.url + "/" + (options.repository.branch || '');
			
			uid = options.repository.username;
			pwd = options.repository.password;
		}

		var svnarg;
		if (uid !== "" && pwd !== "") {
			grunt.log.writeln("Username: " + uid);
			svnarg = ['co', checkoutUrl, out, '--username', uid, '--password', pwd, '--no-auth-cache', '--non-interactive'];
		}
		else if (uid === "" && pwd === "") {
			svnarg = ['co', checkoutUrl, out];
		}

		try {
			which.sync('svn');
		}
		catch(err) {
			return done(new Error('Missing svn in your system PATH'));
		}

		grunt.log.writeln("Checkout URL: " + checkoutUrl);

		grunt.util.spawn({
			cmd: 'svn',
			args: ['co', checkoutUrl, out, '--username', uid, '--password', pwd, '--no-auth-cache', '--non-interactive']
		}, function(err) {

			if (err) {
				grunt.log.writeln("Error occured: " + err.message);
				done(false);
			}

			grunt.log.writeln("Update: " + out);

			grunt.util.spawn({
				cmd: 'svn',
				args: ['update', out]
			}, done);

		});
	});
};
