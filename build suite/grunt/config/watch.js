module.exports = {
  css: {
	  files: ['<%= instance["watch.path"] %>/**/cartridge/scss/**'],
	  tasks: ['sass:dev']
  },
  js: {
	  files: ['<%= instance["watch.path"] %>/**/cartridge/js/**'],
	  tasks: ['browserify:dev']
  },
	options: {
		nospawn: true,
		forever: false
	}
};