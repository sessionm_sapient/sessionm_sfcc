module.exports = function(grunt) {
	var bmConfig = grunt.file.readJSON('grunt/config/dw_bm_config.json');
	return {
		options: {
			server: 'https://<%= instance["webdav.server"] %>',
			interval: 10000
		},
		content: {
			options: {
				archiveName: '<%= settings["content.archive.name"] %>.zip',
				path: bmConfig.impex.site.path,
				selector: bmConfig.impex.site.selector,
				processLabel: bmConfig.impex.site.importLabel
			}
		},
		config: {
			options: {
				archiveName: '<%= settings["config.archive.name"] %>.zip',
				path: bmConfig.impex.site.path,
				selector: bmConfig.impex.site.selector,
				processLabel: bmConfig.impex.site.importLabel
			}
		},
		customMetaValidation: {
			options: {
				archiveName: 'custom-objecttype-definitions.xml',
				path: bmConfig.impex.meta.path,
				selector: bmConfig.impex.meta.selector,
				processLabel: bmConfig.impex.meta.validationLabel
			}
		},
		systemMetaValidation: {
			options: {
				archiveName: 'system-objecttype-extensions.xml',
				path: bmConfig.impex.meta.path,
				selector: bmConfig.impex.meta.selector,
				processLabel: bmConfig.impex.meta.validationLabel
			}
		},
		systemMetaImport: {
			options: {
				archiveName: 'system-objecttype-extensions.xml',
				path: bmConfig.impex.meta.path,
				selector: bmConfig.impex.meta.selector,
				processLabel: bmConfig.impex.meta.importLabel
			}
		},
		customMetaImport: {
			options: {
				archiveName: 'custom-objecttype-definitions.xml',
				path: bmConfig.impex.meta.path,
				selector: bmConfig.impex.meta.selector,
				processLabel: bmConfig.impex.meta.importLabel
			}
		},
		exportUnits: {
			options: {
				archiveName: '<%= generatedExportConfigName %>.zip',
				path: bmConfig.impex.site.path,
				selector: bmConfig.impex.site.selector,
				processLabel: bmConfig.impex.site.exportLabel
			}
		}
	};
};
