module.exports = {
	build: {
		options: {
			style: 'expanded',
			sourceMap: true
		},
		// Switch to using maps of files in config.json
		files: [{
			expand: true,
			cwd: 'output/code/<%= version %>/<%= grunt.option("build.cartridge") %>/',
			src: '**/<%= instance.sass.sourcePath %>/*.scss',
			ext: '.css',
			dest: 'output/code/<%= version %>/<%= grunt.option("build.cartridge") %>/',
			_path: '<%= instance.sass.sourcePath %>',
			rename: function (dest, src) {
				var fileName = src.substring(src.lastIndexOf('/') + 1);
				var path = src.substring(0, src.lastIndexOf('/'));
				path = path.replace(this._path, '<%= instance.sass.sourceStaticReplace %>');
				
				return dest +  path + '/<%= instance.sass.sourceTarget %>/' + fileName;
			}
		}]
	},
	app_proactiv_PG: {
		options: {
			style: 'expanded',
			sourceMap: true
		},
		// Switch to using maps of files in config.json
		files: [{
			expand: true,
			cwd: 'output/code/<%= version %>/app_proactiv_PG/',
			src: '**/<%= instance.sass.sourcePath %>/**/*.scss',
			ext: '.css',
			dest: 'output/code/<%= version %>/app_proactiv_PG/',
			_path: '<%= instance.sass.sourcePath %>',
			rename: function (dest, src) {
				var fileName = src.substring(src.lastIndexOf('/') + 1);
				var path = src.substring(0, src.lastIndexOf('/'));
				path = path.replace(this._path, '<%= instance.sass.sourceStaticReplace %>');
				
				return dest +  path + '/<%= instance.sass.sourceTarget %>/' + fileName;
			}
		}]
	},
	campaigns: {
		options: {
			style: 'expanded',
			sourceMap: true
		},
		// Switch to using maps of files in config.json
		files: [{
			expand: true,
			cwd: 'output/code/<%= version %>/<%= grunt.option("build.cartridge") %>/',
			src: '**/sass/campaigns/**/*.scss',
			ext: '.css',
			dest: 'output/code/<%= version %>/<%= grunt.option("build.cartridge") %>/',
			_path: 'sass/campaigns',
			rename: function (dest, src) {
				var fileName = src.substring(src.lastIndexOf('/') + 1);
				var path = src.substring(0, src.lastIndexOf('/'));
				path = path.replace(this._path, 'css');
				
				return dest +  path + '/' + fileName;
			}
		}]
	},
	content: {
		options: {
			style: 'expanded',
			sourceMap: true
		},
		// Switch to using maps of files in config.json
		files: [{
			expand: true,
			cwd: 'output/code/<%= version %>/<%= grunt.option("build.cartridge") %>/',
			src: '**/sass/partials/common-pages/*.scss',
			ext: '.css',
			dest: 'output/code/<%= version %>/<%= grunt.option("build.cartridge") %>/',
			_path: 'sass',
			rename: function (dest, src) {
				var fileName = src.substring(src.lastIndexOf('/') + 1);
				var path = src.substring(0, src.lastIndexOf('/'));
				path = path.replace(this._path, 'css');
				
				return dest +  path + '/' + fileName;
			}
		}]
	},
	cart: {
		options: {
			style: 'expanded',
			sourceMap: true
		},
		// Switch to using maps of files in config.json
		files: [{
			expand: true,
			cwd: 'output/code/<%= version %>/<%= grunt.option("build.cartridge") %>/',
			src: ['**/sass/cart/splitcart/*.scss','**/sass/cart/inlinecart/*.scss'],
			ext: '.css',
			dest: 'output/code/<%= version %>/<%= grunt.option("build.cartridge") %>/',
			_path: 'sass',
			rename: function (dest, src) {
				var fileName = src.substring(src.lastIndexOf('/') + 1);
				var modifiedFileName = fileName.substring(0,fileName.lastIndexOf('.'))+ '-styles'+ '.css';
				var path = src.substring(0, src.lastIndexOf('/'));
				path = path.replace(this._path, 'css');
				
				return dest +  path + '/' + modifiedFileName;
			}
		}]
	},
	dev: {
		options: {
			style: 'expanded',
			sourceMap: true
		},
		files: [{
			expand: true,
			cwd: '<%= instance["watch.path"] %>/',
			ext: '.css',
			src: '**/<%= instance.sass.sourcePath %>/**/<%= instance.sass.sourceFile %>',
			dest: '<%= instance["watch.path"] %>/',
			rename: function(dest, src) {
				var fileName = src.substring(src.lastIndexOf('/') + 1);
				var path = src.substring(0, src.lastIndexOf('/'));
				path = path.replace('scss', 'static');
				
				return dest +  path + '/css/' + fileName;
			}
		}]
	}
};