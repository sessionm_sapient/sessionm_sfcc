module.exports = {
	options: {
		method: 'POST',
		headers: {
			'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
			'Accept-Encoding': 'gzip,deflate',
			'Accept-Language': 'de-de,de;q=0.8,en-us;q=0.5,en;q=0.3',
			'Connection': 'keep-alive',
			'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0'
		}
	},
	unzipUpload: {
		/* Set in lib/generate_config */
	},
	createMetaDirectory: {
		options: {
			url: 'https://<%= instance["webdav.server"] %><%= instance["webdav.meta_import.root"] %>',
			form: {
				method: 'MKCOL'
			},
			auth: {
				user: '<%= instance["webdav.username"] %>',
				pass: '<%= instance["webdav.password"] %>'
			},
			ignoreErrors: true
		}
	},
	unzipMeta: {
		options: {
			url: 'https://<%= instance["webdav.server"] %><%= instance["webdav.meta_import.root"] %><%= settings["meta.archive.name"] %>.zip',
			form: {
				method: 'UNZIP'
			},
			auth: {
				user: '<%= instance["webdav.username"] %>',
				pass: '<%= instance["webdav.password"] %>'
			}
		}
	},
	deleteUpload: {
		/* Set in lib/generate_config */
	},
	login: {
		options: {
			url: 'https://<%= instance["webdav.server"] %>/on/demandware.store/Sites-Site/default/ViewApplication-ProcessLogin',
			form: {
				LoginForm_Login: '<%= instance["webdav.username"] %>',
				LoginForm_Password: '<%= instance["webdav.password"] %>',
				LoginForm_RegistrationDomain: 'Sites'
			},
			jar: true,
			followRedirect: true,
			ignoreErrors: true
		}
	},
	deleteCodeVersion: {
		options: {
			url: 'https://<%= instance["webdav.server"] %><%= instance["webdav.cartridge.root"] %><%= version %>',
			ignoreErrors: true,
			form: {
				method: 'DELETE'
			},
			auth: {
				user: '<%= instance["webdav.username"] %>',
				pass: '<%= instance["webdav.password"] %>'
			}
		}
	},
	createCodeVersion: {
		options: {
			url: 'https://<%= instance["webdav.server"] %><%= instance["webdav.cartridge.root"] %><%= version %>',
			form: {
				method: 'MKCOL'
			},
			auth: {
				user: '<%= instance["webdav.username"] %>',
				pass: '<%= instance["webdav.password"] %>'
			}
		}
	},
	activateCodeVersion: {
		options: {
			url: 'https://<%= instance["webdav.server"] %>/on/demandware.store/Sites-Site/default/ViewCodeDeployment-Activate',
			form: {
				CodeVersionID: '<%= version %>'
			},
			jar: true
		}
	},
	importContent: {
		// Import initial content package. Should be done after a dbinit.
		// Status of import can be checked with dw_bm_checkprogress:content.
		options: {
			url: 'https://<%= instance["webdav.server"] %>/on/demandware.store/Sites-Site/default/ViewSiteImpex-Dispatch',
			form: {
				ImportFileName: '<%= settings["content.archive.name"] %>.zip',
				import: 'OK',
				realmUse: 'true'
			},
			jar: true
		}
	},
	validateCustomMeta: {
		options: {
			url: 'https://<%= instance["webdav.server"] %>/on/demandware.store/Sites-Site/default/ViewCustomizationImport-Dispatch',
			form: {
				SelectedFile: 'custom-objecttype-definitions.xml',
				ProcessPipelineName: 'ProcessObjectTypeImport',
				ProcessPipelineStartNode: 'Validate',
				JobDescription: 'Validate+systemmeta+data+definitions',
				JobName: 'ProcessObjectTypeImpex',
				validate: ''
			},
			jar: true
		}
	},
	validateSystemMeta: {
		options: {
			url: 'https://<%= instance["webdav.server"] %>/on/demandware.store/Sites-Site/default/ViewCustomizationImport-Dispatch',
			form: {
				SelectedFile: 'system-objecttype-extensions.xml',
				ProcessPipelineName: 'ProcessObjectTypeImport',
				ProcessPipelineStartNode: 'Validate',
				JobDescription: 'Validate+custommeta+data+definitions',
				JobName: 'ProcessObjectTypeImpex',
				validate: ''
			},
			jar: true
		}
	}
};
