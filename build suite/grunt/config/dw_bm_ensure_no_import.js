module.exports = function(grunt) {
	var bmConfig = grunt.file.readJSON('grunt/config/dw_bm_config.json');
	return {
		options: {
			server: 'https://<%= instance["webdav.server"] %>'
		},

		content: {
			options: {
				archiveName: '<%= settings["content.archive.name"] %>.zip',
				path: bmConfig.impex.site.path,
				selector: bmConfig.impex.site.selector,
				processLabel: bmConfig.impex.site.importLabel
			}
		},

		config: {
			options: {
				archiveName: '<%= settings["config.archive.name"] %>.zip',
				path: bmConfig.impex.site.path,
				selector: bmConfig.impex.site.selector,
				processLabel: bmConfig.impex.site.importLabel
			}
		}
	};
};
