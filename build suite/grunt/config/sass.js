module.exports = {
	build: {
		options: {
			style: 'expanded',
			sourceMap: true
		},
		// Switch to using maps of files in config.json
		files: [{
			expand: true,
			cwd: 'output/code/<%= version %>/',
			src: '**/<%= instance.sass.sourcePath %>/**/<%= instance.sass.sourceFile %>',
			ext: '.css',			
			dest: 'output/code/<%= version %>/',
			_path: '<%= instance.sass.sourcePath %>',
			rename: function (dest, src) {
				var fileName = src.substring(src.lastIndexOf('/') + 1);
				var path = src.substring(0, src.lastIndexOf('/'));
				path = path.replace(this._path, 'static');
				
				return dest +  path + '/css/' + fileName;
			}
		}]
	},
	dev: {
		options: {
			style: 'expanded',
			sourceMap: true
		},
		files: [{
			expand: true,
			cwd: '<%= instance["watch.path"] %>/',
			ext: '.css',
			src: '**/<%= instance.sass.sourcePath %>/**/<%= instance.sass.sourceFile %>',
			dest: '<%= instance["watch.path"] %>/',
			rename: function(dest, src) {
				var fileName = src.substring(src.lastIndexOf('/') + 1);
				var path = src.substring(0, src.lastIndexOf('/'));
				path = path.replace('scss', 'static');
				
				return dest +  path + '/css/' + fileName;
			}
		}]
	}
};