module.exports = {
	all: {
		files: [{
			expand: true,
			cwd: 'output/code/<%= version %>/',
			src: ['**/cartridge/static/**/*.css'],
			dest: 'output/code/<%= version %>/',
			ext: '.min.css'
		}]
	}
	/* Additional targets created in lib/generate_config */
};