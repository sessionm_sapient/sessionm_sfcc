module.exports = {
	build: {
		options: {
			config: '<%= grunt.option("build.options.config") %>',
			basePath: '<%= grunt.option("build.options.basePath") %>'
		}
	}
};