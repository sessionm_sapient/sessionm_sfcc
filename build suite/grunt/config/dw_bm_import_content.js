module.exports = function() {
	return {
		options: {
			server: 'https://<%= instance["webdav.server"] %>',
			archiveName: '<%= settings["content.archive.name"] %>.zip'
		},
		default: {
		}
	};
};
