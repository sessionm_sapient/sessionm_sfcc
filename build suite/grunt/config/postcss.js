module.exports = {
	options: {
		processors: [
			require('autoprefixer')({})
		]
	},
	build: {
		files: [{
			expand: true,
			cwd: 'output/code/<%= version %>/',
			src: '**/*.css',
			dest: 'output/code/<%= version %>/'
		}]
	},
	dev: {
		files: [{
			expand: true,
			cwd: '<%= instance["watch.path"] %>/',
			src: '*/cartridge/static/*/css/**/*.css',
			dest: '<%= instance["watch.path"] %>/'
		}]
	}
};
