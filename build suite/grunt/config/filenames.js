module.exports = {
	files: {
		options: {
			valid: /^[a-z0-9_]+.(.js|.yaml|.json)$/
		},
		src: [
			'grunt/**/*',
			'unit_tests/**/*',
			'!grunt/config/mochaTest.js'
		],
		filter: 'isFile'
	},
	directories: {
		options: {
			valid: /^[a-z0-9_]+$/
		},
		src: '<%= filenames.files.src %>',
		filter: 'isDirectory'
	}
};
