module.exports = {
	options: {
		release_path: '<%= instance["webdav.cartridge.root"] %><%= version %>/<%= version %>.zip',
		basic_auth: true,
		overwrite_release: true,
		two_factor: '<%=instance["webdav.two_factor"] %>',
		two_factor_p12: '<%=instance["webdav.two_factor.cert"] %>', //path to p12 (pfx) file
		two_factor_password: '<%=instance["webdav.two_factor.password"] %>',
		two_factor_url: 'https://<%=instance["webdav.server.cert.url"]%>',
		url: 'https://<%= instance["webdav.server"] %>'
	},
	code: {
		/* Set in lib/generate_config */
	},
	cartridges: {
		files: {
			src: 'output/code/<%= version %>.zip'
		}
	},
	siteImport: {
		options: {
			release_path: '<%= instance["webdav.site_import.root"] %><%= settings["config.archive.name"] %>.zip'
		},
		files: {
			src: 'output/site_import/<%= settings["config.archive.name"] %>.zip'
		}
	},
	siteMeta: {
		options: {
			release_path: '<%= instance["webdav.meta_import.root"] %><%= settings["meta.archive.name"] %>.zip'
		},
		files: {
			src: 'output/site_import/<%= settings["meta.archive.name"] %>.zip'
		}
	}
};
