module.exports = {
	all: {
		files: [{
			expand: true,
			cwd: 'output/code/<%= version %>/',
			src: ['**/cartridge/static/**/*.js', '!*.min.js', '!**/lib/**/*.js'],
			dest: 'output/code/<%= version %>/'
		}]
	}
	/* Additional targets created in lib/generate_config */
};
