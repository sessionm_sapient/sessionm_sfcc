module.exports = function() {
	return {
		options: {
			server: 'https://<%= instance["webdav.server"] %>',
			archiveName: '<%= settings["config.archive.name"] %>.zip'
		},
		default: {
		}
	};
};
