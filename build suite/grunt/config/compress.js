module.exports = {
	code: {
		/* Set in lib/generate_config */
	},
	cartridges: {
		options: {
			archive: 'output/code/<%= version %>.zip'
		},
		files: [{
			src: ['**/*'],
			cwd: '<%= instance["site_cartridge.path"]%>',
			dist: 'output/code',
			expand: true
		}]
	},

	/* package all contents except meta data, ignore any generated zip file contents*/
	siteImport: {
		options: {
			archive: 'output/site_import/<%= settings["config.archive.name"] %>.zip'
		},
		files: [{
			src: ['**/<%= settings["config.archive.name"] %>/**/*', '!**/*.zip'],
			cwd: 'output/site_import/',
			expand: true
		}]
	},

	/* package only meta data*/
	siteMeta: {
		options: {
			archive: 'output/site_import/<%= settings["meta.archive.name"] %>.zip'
		},
		files: [{
			src: ['**/meta/*'],
			cwd: 'output/site_import/site_init/',
			flatten: true,
			expand: true
		}]
	}
};
