module.exports = function() {
	'use strict';

	return {
		// validate all json files
		all: {
			src: [
				'*.json',
				'grunt/**/*.json',
				'unit_tests/**/*.json'
			]
		}
	};
};
