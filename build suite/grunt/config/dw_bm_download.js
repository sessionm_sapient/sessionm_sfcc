module.exports = {
	options: {
		server: 'https://<%= instance["webdav.server"] %>',
		login: '<%= instance["webdav.username"] %>',
		password: '<%= instance["webdav.password"] %>',
		debug: false // debug http requests
	},
	default: {}
};
