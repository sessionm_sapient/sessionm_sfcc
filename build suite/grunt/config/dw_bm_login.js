module.exports = function() {
	return {
		options: {
			server: 'https://<%= instance["webdav.server"] %>',
			username: '<%= instance["webdav.username"] %>',
			password: '<%= instance["webdav.password"] %>'
		},

		default: { }
	};
};
