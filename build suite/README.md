#Build Suite

[Looking for the ant version?](https://bitbucket.org/demandware/build-suite-ant/overview)  Please note ANT version is being sunsetted in favor of the newer Grunt based build suite.

The build suite is powered by [Grunt](http://gruntjs.com) (v4.4.x or newer) - a popular and well-tested Javascript task runner. The prototype is compatibility with the major ANT target tasks and the existing properties files. 

## Introduction
The Build Suite is an easy-to-use set of scripts to automate your build processes and adding some additional value to it. In a nutshell you configure your SVN and/or Git locations, your target environment (i.e. staging) and then hit the button to package a build and to deploy it to your environment and activate the new version.

It is part of the [Community Suite](https://xchange.demandware.com/community/developer/community-suite) set of tools.


## Changelog

### [unreleased]
#### Changed:
- Fixed code quality issues (regarding linting the Build Suite itself), renamed config files (solved #35, #36)
- Changed Documentation (widely updated and improved)
- Changed SASS configuration, improved folder structure
#### Fixed:
- Restored "gitfetch" task in order to reduce update time for Git repositories
- Bugfix regarding repository names with dots (solved #9)
- Fixed JS/CSS merging (solved #38)
- Fixed syntax in config example files (solved #37)

### [[1.0.0]](https://bitbucket.org/demandware/build-suite/commits/tag/1.0.0) - 2016-04-26 
#### Removed:
- unused configuration parameters in configuration files
#### Changed:
- updated configuration file structure to benefit from JSON format
- restructured (demo) site import, extended functionality and configurability
#### Added:
- proper default values for less used configuration parameters
- documented copies of configuration files
- additional log output in case of errors to improve usability

### [[Grunt Branch]](https://bitbucket.org/demandware/build-suite/src/4ad8e2001b5a4200422ced9844df545dd6f45ae5/?at=grunt) - 2015-02-27
- Merged new features in to grunt branch including support for svn and two factor auth.
- IMPORTANT: ALL properties files are being moved to json files to support more advanced configuration.  A merge option exists to merge the global properties files.  The project repository folders should be converted by hand if you use any advanced options.  A sample file is provided. 
- IMPORTANT: Run `npm cache clean` `npm install` as we are now including a lint module. If you can't get it to clone because of an authentication problem. Open the package.json and change `"dslint": "git+https://bitbucket.org/demandware/dslint.git",` to `"dslint": "git+https://[user]:[password]@bitbucket.org/demandware/dslint.git"`,
- Known Issues: Two factor auth will successfully push the archive to the remote server.  It is a known issue that it will not unzip nor activate.  We are working on that.


## Support / Contributing
Feel free to create issues and enhancement requests or discuss on the existing ones, this will help us understanding in which area the biggest need is. For discussions please start a topic on the [Community Suite discussion board](https://xchange.demandware.com/community/developer/community-suite/content).

## License
Licensed under the current NDA and licensing agreement in place with your organization. (This is explicitly not open source licensing.)


## Getting started

*See also: [Grunt Getting Started](http://gruntjs.com/getting-started).*

**Please be sure to use a current Node.js version (4.4.x or newer)**

### Quick Start

- Rename `build/config.json.sample` to `config.json`.
- Rename  `build/projects/sample/config.json.min-sample` to config.json, rename sample folder to match project name
- Edit `build/projects/sample/config.json` to include your repositories and configure for your deployment environment
- Edit `build/config.json`, make `build.project.name` match your folder name and set the code version name that will be used - `build.project.version`   

- Install Grunt and resolve dependencies:
	1. `npm install grunt-cli`
	1. `npm install`
	1. `grunt dist` - by default, `dist` task runs `init`, `build`, `upload` and `activate`. For a list of available grunt tasks, run `grunt --help`.



### Contents of the cartridge

    :::text
     -root
      |-build
      | |-jenkins 								Sample config for jenkins (coming soon)
      | |-projects
      | | `-sample								Folder names here represent the project name
      | |   |-config.json                       Consolidates all previous property files in this directory
      | |   |-config.json.min-sample            Minimum possible configuration template. Copy to config.json for basic use.
      | |   |-config.json.min-doc               Documentation for minimum config. Do NOT use as template!
      | |   |-config.json.full-sample           Full configuration template. Copy to config.json for advanced use.
      | |   `-config.json.full-doc              Full documentation for config. Do NOT use as template!
      | |
      | |-config.json                 			Replaces prior build.properties file
      | `-config.json.sample         			Template.  Copy this to config.json
      |
      |-grunt									Grunt Configuration and tasks
      | |-config 								Alias (alias.yml) and Configuration parameters (*.js)
      | |-lib									Miscellaneous Utilities
      | `-tasks									All Demandware specific task plugins
      |
      |-resources								Sample template for storefront toolkit
      |
      |-Gruntfile.js							Main Gruntfile 
      `-package.json							Node Package Manager dependencies






### Quick Start Tips

#### Need to specific a version number or want have multiple source projects?

Specific parameters on the command line. This just works for build.project.version and a new property called build.project.number

`grunt dist --build.project.version=sg_community --build.project.number=14.8`

####Need to use a folder other than `cartridges` for my code?

In `build/projects/[projectname]/config.json` file, set repository property `repository.source.path` to match your source folder name.

####Need to specify which directories in your project to upload?

In `build/projects/[projectname]/config.json` file, set repository property `repository.siteImport.glob` to select desired files/directories.

By default, it is set to upload everything `**/*`. It uses grunt's [globbing pattern](http://gruntjs.com/configuring-tasks#globbing-patterns), except for `{}` due to the limitation of Java Properties file.

For example, to ignore a file, set `glob` to `['**/*','!filename']`.
To ignore a folder, set `glob` to `['**/*','!**/folder/**']`. 

#### Use with Eclipse

Eclipse Running as: http://paultavares.wordpress.com/2014/01/06/setup-an-eclipse-run-external-tool-configuration-for-grunt/

### Available Tasks

All main targets are defined in the aliases.yml build file. Call `grunt --help` to receive a list of available tasks.

#### Build:

* `grunt build` performs a checkout and local build of the complete project
* `grunt upload` performs code upload of previously built project
* `grunt activate` activates the configured code version
* `grunt dist` performs build, upload, activate and cleans up uploaded file (call `grunt http:deleteUpload` to do this manually). 

#### Site Import:

An import of site configuration can be done after the build or dist task. 
To configure Site import, see [documented Config example](https://bitbucket.org/demandware/build-suite/src/bb1dd935bfe56021f828cc9872c0dfde1bedf18e/build/projects/sample/config.json.full-doc.js?at=master&fileviewer=file-view-default) and check against assumed folder structure below. The site import can be divided into two parts: 1. Configuration/Initialization and 2. Demo Site data (optional).

* `grunt initSite`: Site initialization/configuration import (default source folder: sites/site_template)
* `grunt initDemoSite`: Demo site import (default source folder: sites/site_demo)
* `grunt importSite`: Complete site import, including demo site (if given)
* `grunt importMeta`: Metadata import. Metadata is always read from configuration data (default: sites/site_template/meta)


### Repository structure

The build system assumes a certain structure within an SVN repository:

    :::text
     -project                       (all files for this project)
      |-branches                     (optional branches)
      |-tags                         (tags that were created on trunk or a branch)
      | |-1_0_0                      (sample tag on trunk or a branch)
      | | |-cartridges
      | | `-sites
      | |
      | `-1_1_0                     (another sample tag)
      |
      `-trunk                        (trunk, the main development branch)
        |-cartridges                 (all Demandware cartridge projects)
        | |-app_sample
        | |-bm_plugin
        | '-...
        |
        `-sites
          |-site_template            (site template structure as defined for site import)
          | |-custom-objects
          | |-meta
          | `-sites
          |   |-site_a               (actual site definitions)
          |   `-site_b               (actual site definitions)
          |-site_demo
          | |-catalogs               (demo site data, example products/customer accounts etc.)
          | |-pricebooks
          | `-sites
          |   |-site_a               (actual site definitions)
          |   `-site_b               (actual site definitions)
          `-config                   (optional: target-instance based configurations that perform replace operations on init/demo data)
            |-dev_a                  (environment config folder)
            |-dev_b
            |-stg
            `-prd


This is the structure assumed for a Git repository:

    :::text
     -root
      |-cartridges                 (all Demandware cartridge projects)
      | |-app_sample
      | |-...
      | `-bm_plugin
      |
      `-sites
        |-site_template            (site template structure as defined for site import)
        | |-custom-objects
        | |-meta
        | `-sites
        |   |-site_a               (actual site definitions)
        |   `-site_b               (actual site definitions)
        |-site_demo
        | |-catalogs               (demo site data, example products/customer accounts etc.)
        | |-pricebooks
        | `-sites
        |   |-site_a               (actual site definitions)
        |   `-site_b               (actual site definitions)
        `-config                   (optional: target-instance based configurations that perform replace operations on init/demo data)
          |-dev_a                  (environment config folder)
          |-dev_b
          |-stg
          `-prd


### Documentation

#### Detailed Steps to run a build

   1. Copy `config.json.sample`, rename it `config.json` and replace the sample values with your configuration settings. 

      You can configure multiple projects to be processed from the same build suite installation. The `build.project.name` property defines the project that is currently built.

     -  build.project.name - project name, selects the `build_cs\cartridge\build\projects\${build.project.name}` directory. 
     -  build.project.version - this maps to the code version that is created and activated on the server
   
   2. Modify dependency file settings. Copy sample folder to match `build.project.name` and use one of the sample files as a templated for ```projects\${build.project.name}\config.json```.

      The dependency file can multiple SVN and/or Git repositories. Each repository can be configured separately.
      The Build Suite currently supports **SVN and Git**. See below for detailed structure of the configuration.

   3. Modify environment settings. For the ```webdav.server``` property be sure to use the Demandware hostname with the dashes so that you do not run into any SSL exceptions when connecting to the target environment with the Build Suite. E.g.: ```instance-realm-customer.demandware.net```.

   4. If you intend to export code from Git repositories, then you must first install Git locally
      on your machine and ensure it runs from the command line.

   5. If you want to use Git SSH URIs (to enable the faster "archive" git export command), then you need to setup the Git SSH key accordingly.
      You will need to set up the environment variable HOME pointing to your Git home directory where the .ssh directory
      exists (e.g. HOME=%USERPROFILE%) to make it possible for Git to find the SSH Key, and you must afterwards upload your key to your Git host.
      See http://git-scm.com/book/en/Git-on-the-Server-Generating-Your-SSH-Public-Key.

      If your ssh key contains a passphrase (optional), you will need to use a password saver like ssh_agent so that you are not prompted
      for your passphrase which will hang the script when running in the UX Studio Plugin.
      See https://help.github.com/articles/working-with-ssh-key-passphrases.

      Also it is recommended that you set the GIT_SSH environment variable to the ssh executable that was
      included in your Git installation to avoid problems with the build script from hanging in UX Studio Plugin.

   6. Run one or more tasks, e.g. `grunt dist` (see above for details).


#### Repository configuration

Multiple repositories can be configured in the project configuration files (e.g. projects/sample/config.json). You will have to modify the "dependencies" array. The repository configuration structure is as follows:

     "dependencies": [
        {
          "repository": {
            "url": "https://bitbucket.org/demandware/sitegenesis.git",             URIs Can be SSH or HTTPS as well as absolute ('file://C:/...') or relative ('..') local paths. 
            "type": "git",                                                         git|svn|file (Existing Git repositories will be fetched instead of cloned again.)
            "branch" : "master",                                                   Branch or Tag name (optional)
            "username": "anonymous",                                               Username for SSH or HTTPS auth (we recommend using PSK though)
            "password": "secure"                                                   Password for SSH or HTTPS auth 
          },
          "source": {
            "path": "cartridges",                                                  Cartridges folder name (optional). IMPORTANT: This defaults to "cartridges", so you will have to enter "." to use the root directory.
            "glob": "**/*",                                                        Globbing pattern for source files (optional)
            "ignoreEmpty": false                                                   If set to true, no error is raised if no source files were found (optional)
          },
          "siteImport": {
            "enabled": "true",                                                     Enable/disable Site import for this repository (optional), defaults to true
            "initPath": "sites/site_template",                                     Name of source folder used for site configuration import (optional), displayed value is default
            "demoPath": "sites/site_demo",                                         Name of source folder used for demo site import (optional), displayed value is default
            "instancePath": "sites/config"                                         Instance configuration folder
          },
          "includeCartridges": [                                                   List of Cartridges that are included in the build. Enter folder names here.
            "app_storefront_core"
          ]
        }
     ]


#### Version numbers

  The version numbers should contain 3 digits i.e. ```1_0_0``` where the first digit represents
  a major release, the second a minor point release/update and the third digit a hot fix
  update i.e. if you are doing multiple releases to UAT fixing small issues you will only
  update the bug fix/update number, if that passes release and is a small update then the
  minor revision number is updated and a new tag created. Release numbers should be
  separated by _ only.

### Site import

(Documentation to be completed soon)

#### Site template

  The site template is based on the site structure contained within the 
  site import zip files. As each project contains a site template structure the
  build process commbines them all by copying the contents into the same directory
  and then zips it up. The idea behind this is that each project may define their
  own site and custom object definitions (if they require ones on top of those defined
  in a common cartridge) that will allow the site to be deployed and custom config
  loaded in.
  It's recommended to have just configuration inside the site template structure and not content/catalog 
  data to keep it manageable inside version control and to keep the build small (there is a max upload size limit of 100MB over webdav), content and catalog can can be imported from the staging instance.

The build zip of site_template configuration data is only created when config setting "build.project.codeonly" is set to false.

### Features

#### Linting [Server Side ds files]

Server side linting is performed by the suite as part of the build phase.  The library TSLint is now a [Community Suite Repository](https://bitbucket.org/demandware/dslint/overview). 

Success: Running "dw_dslint:files" (dw_dslint) task
>> 100 files lint free.

Failure: Running ...
>> failed 10 in 100 files 

#### Sass

By default, the `build` task looks for a `style.scss` file in the `scss` directory of every cartridge. The output, the complied `style.css` is put in `static/default/css` in the same cartridge. 
Two parts of this process are configurable in `build/projects/[projectname]/config.json`: You can change the file name from `style.scss` via `environment.sass.sourceFile` (target filename will always be the same with .css ending).
You can also change the source directory from `scss` to anything you want (e.g. `sass\source`). Since Site Genesis proposes a fixed folder structure for CSS files, the target directory is not configurable.
Also, since there usually is only one front-end cartridge providing CSS, these properties are not configurable per cartridge but in a global manner only.

#### Autoprefixer
[Autoprefixer](https://github.com/postcss/autoprefixer) parses CSS and adds vendor-prefixed CSS properties using the [Can I Use](http://caniuse.com) database.
The `build` task runs `autoprefixer` on all `.css` files. By default, `autoprefixer` target these browsers: `> 1%, last 2 versions, Firefox ESR, Opera 12.1`. The ability to configure specific browser targets will be added in the future.

#### Resource optimization

The Build Suite allows you to concatenate and minify your CSS and Javascript resources.

1. specify the cartridges concatenation should apply for in your build/config.json (static.files.cartridges)
1. enable concatenation in your build/config.json (static.files.concatenate)
1. include tags in your templates to mark which files you want to concatenate


##### Optimization before concatenation

Most JavaScript libraries (such as jQuery) are already available as minified version. These projects use UglifyJS/Closure Compiler which provides much better compression than YUI Compressor. An additional minify process can result growing file sizes. At the moment optimization is done after concatenation so there is no chance to exclude files from minification process. Also the license comments included in several libraries will be cut off the file. With the modified version it is possible to set the build property ```build.optimize.beforeconcat=true```

Additionally there is the option to exclude additional file patterns using a property: ```build.optimize.js.excludes=**/jquery-?.?.?.js,**/jquery-?.??.?.js```

This can be useful to keep the unminified versions as they are to use them with source maps. Or to skip warnings of the optimizer (i.e. Closure Compiler prints out warnings when minifying jQuery source)


##### CSS example

```html

<!--- BEGIN CSS files to merge.(source_path=cartridge/static/default;targetfile=css/allinone.css) --->
<link rel="stylesheet" href="${URLUtils.staticURL('/css/example1.css')}" />
<link rel="stylesheet" media="print" href="${URLUtils.staticURL('/css/example2.css')}" />
<link rel="stylesheet" href="${URLUtils.staticURL('/css/example3.css')}" />
<!--- END CSS files to merge. --->
```

##### JS example

```html

<!--- BEGIN JS files to merge.(source_path=cartridge/static/default;targetfile=js/allinone.js) --->
<script src="${URLUtils.staticURL('/lib/example1.js')}" type="text/javascript"></script>
<script src="${URLUtils.staticURL('/lib/jquery/example2.js')}" type="text/javascript"></script>
<!--- END JS files to merge. --->
```

#### Storefront Toolkit Build Info

It is possible to extend the dw storefront toolkit with build information created by this cartridge:

- copy the file ```/build_cs/cartridge/resources/build_info.isml.template``` to ```/app_<shop>/cartridge/templates/default/build/```
- IMPORTANT: also place a copy of the file ```build_info.isml.template``` with the filename ```build_info.isml``` to the same directory, so you should have both ```build_info.isml.template``` and ```build_info.isml``` existing in ```/app_<shop>/cartridge/templates/default/build/```
- copy the file ```/build_cs/cartridge/resources/debug.js``` to your static content and include it on your site
- add ```<isinclude template="build/build_info">``` to your footer
- add your storefront cartridge to ```buildinfo.cartridge``` property in the ```build.properties/config.json``` for your build
- you will see new option "Build Information" in dw storefront toolkit window after build is activated

####  Test Files

  There is a flag in the build called include.test.deployments which can be
  overidden in your build/config.json that can exclude test code from the deployments.
  Any cartridge prefixed with test_ in the name will be excluded and any
  test_site_templates will also be excluded. All test code and test pipelines
  should exist in these test areas so they are not deployed in production
  builds.

#### 2-Factor Authentication

For 2-factor authentication, it will be turned on in your staging instance.  

- Update the url in your webdav connection file to be https://cert.staging.... instead of https://staging....

- Edit the dw-upload.js file: 

```
  two_factor : true,
  two_factor_p12: 'test.p12', //path to p12 (pfx) file relative to root
  two_factor_password: 'test'
```

### Issues/Resolutions

If you've previously installed npm your version of npm and plugins may be out of date.  

Try: npm update npm -g

If that does not help on windows you may want to 

* Use windows installer to re-install node.js. 
* Check your path to ensure you don't have an old version referenced 

http://toby1kenobi.com/2014/03/problems-installing-grunt-contrib-uglify-on-windows-8-1/

Grunt is not found on windows

* Try path=%PATH%;%APPDATA%\npm  then grunt.  If that works then add %APPDATA%\npm to your PATH

### Re-initializing an instance 
   
   To re-create an instance using this tool you will need to do the following:

   1. Execute the dbinit for the site from the control center. This will completely
      erase the existing site data. You may need to re-start the server a couple of
      times before you see the business manager come up correctly. This process can
      take a while. You will also need to wait for your new admin account password
      as that will be re-set by the init process (remember to log in and change
      the password back to original or update environment properties before running
      the build - if you update make sure to get the right Sites-Site password).
      Wait until you got the email with the new credentials before starting the
      instance.

   2. Execute the build with the following targets in sequence:
      - dist

    This will download, upload, and import the site import information stored in version control.
  

### Continuous integration (CI)

The Build Suite can be used to create continuous builds, here is how it can be done with Jenkins CI (but other CI systems will work as well).

1. Install node and Jenkins CI
1. setup your config.json as you like
1. create a new Jenkins project
1. Add a shell task and run the command`grunt dist`
1. If you are creating custom Jenkins environment variables, for example with a build parameter plugin, you will need to export them in the shell before your grunt command to make them available to grunt for replacing:
`export TARGET_ENVIRONMENT=${TARGET_ENVIRONMENT}`  

For GIT builds, in your Build Suite dependency file you must set the path to the local Jenkins workspace and NOT to your remote GIT repository location as Jenkins will export the version to build locally! e.g. ```file://C:/users/me/.jenkins/jobs/sample/workspace```. 

Variables substitution from Jenkins is possible.  This overrides config files with the following syntax to be replaced with their environment variable equivalent:

     ${ENV_VAR_NAME}

https://wiki.jenkins-ci.org/display/JENKINS/Building+a+software+project

Examples:

     "build.project.version": "${BUILD_NUMBER}"  (json config file)
      build.project.version=${BUILD_NUMBER}  (properties file)