/* Copyright (C) 2017 SessionM. All Rights Reserved*/

/*
*
* SMFetchTierDataJob  will fetch tier details and dump these details into tierdata custom object of SFCC
*/
module.exports.SMFetchTierDataJob = function () {
		var ComConst = require('../common/CommonConstants').CommonConstants;
		/*make call to SM retrieve multiple customer service and array of profile smID as a part of request */
		var serviceFacade = require(ComConst.PATH_FACADE+'RewardsFacade');
			var serviceResponse = serviceFacade.FetchSMTierDetails();
			if (!empty(serviceResponse) && !empty(serviceResponse.responseObj) && (serviceResponse.responseObj.status === 'ok')){
				if(serviceResponse.responseObj.tiers.length > 0){
					var CustomObjectMgr = require('dw/object/CustomObjectMgr');
					var	CO=CustomObjectMgr.getAllCustomObjects('tierData');
					if(CO.count > 0){
						 removeExistingCustomObject(CO);
					}
					for each(var tier in serviceResponse.responseObj.tiers){
						addTierDataCustomObjects(tier);
					}
				}
			}else{
				throw new Error('No tier details recieved from SM service');
			}
};
/*
*param sm Tier data response
*returns custom object created in SFCC
*/
addTierDataCustomObjects = function (smTierData){
	var Logger = require('dw/system/Logger');
   	var CustomObjectMgr = require('dw/object/CustomObjectMgr');
   	var Transaction =  require('dw/system/Transaction');
    var CO;
   	//Remove all existing Custom objects
    var UUIDUtils = require('dw/util/UUIDUtils');
    var uuid : String  = UUIDUtils.createUUID();
	if(null !== smTierData){
		var CommonConstants = require('../common/CommonConstants').CommonConstants;
 		var sfccTierMap = CommonConstants.SFCC_TIER_MAP;
 		Transaction.wrap(function(){
 			CO = CustomObjectMgr.createCustomObject('tierData', uuid);
			       	for each(var entry in sfccTierMap ){
			       		if(entry.value in smTierData){
			       		   if(entry.key.equals('smMultiplier')){
			       			CO.custom[entry.key] = new Number(smTierData[entry.value]);
			       		   }else{
			       			CO.custom[entry.key] = smTierData[entry.value];   
			       		   }	
			       	}
			  }
		});
	}
};
/*
*param customObjects
*returns remove custom object from SFCC
*/
removeExistingCustomObject = function(customObjects){
	var Transaction =  require('dw/system/Transaction');
	var CustomObjectMgr = require('dw/object/CustomObjectMgr');
	 Transaction.wrap(function(){
		for each(var co in customObjects){
			CustomObjectMgr.remove(co);
		}
	 });
};
