/* Copyright (C) 2017 SessionM. All Rights Reserved */

/* 
* uploadFilesToSFTP .ds  upload file to SFTP
*/
function uploadFilesToSFTP()
{
	var index = 0;
	var	copyResult;
	var	filePattern = arguments[index].get('filePattern');
	var	sourceFolder = arguments[index].get('sourceFolder');
	var	targetFolder = arguments[index].get('targetFolder');
	var	errorMsg = '';
	var Logger = require('dw/system/Logger');
	var svc  = require('dw/svc');
	var service  =  svc.ServiceRegistry.get('int_sessionm_sftpservice');
	var configuration =	service.getConfiguration();
	var credentials = configuration.getCredential();
	var user = credentials.getUser();
	var password= credentials.getPassword();
	var url = credentials.getURL();
    try{
		//Test mandatory paramater
		if(empty(url) && empty(user) && empty(password) &&  empty(sourceFolder) && empty(targetFolder))
		{
			errorMsg = 'one or more mandatory parameters are missing.';
			Logger.error( errorMsg);
			throw new Error(errorMsg);
		}
		var result ={};
		service.setOperation('connect',url,user,password);
		result = service.call();
		if('OK'.equalsIgnoreCase(result.status))
		{
			 if(!empty(filePattern))
			 {
			   filePattern = filePattern;
			 }
			 else
			 {
				var CommonConstants = require('../common/CommonConstants').CommonConstants;
				filePattern = CommonConstants.XML_FILE_PATTERN;
			 }
			    copyResult = copyFilesToTarget(service, sourceFolder, filePattern, targetFolder);
		}
		else
		{
		    errorMsg = 'the connection couldnt be established.';
			Logger.error('the connection couldnt be established.');
			throw new Error(errorMsg);
		}
		if(!copyResult.done )
		{
			Logger.error(copyResult.message);
			throw new Error(copyResult.message);
		}
   }catch(e){
   	  Logger.error('Error in uploading the file '+ e);
   }
   finally{
   	    service.setOperation('disconnect');
		result = service.call();
		return;
   }
}


/**
*	Copy (and archive locally) files to the remote FTP-Target-Folder
**/
function copyFilesToTarget(service , sourceFolder , filePattern , targetFolder : string) {
	var targetFolderStr = targetFolder.charAt(0) === '/' ? targetFolder.substring(1) : targetFolder;
	var sourceDirStr = sourceFolder.charAt(0).equals('/') ? sourceFolder + '/'  : '/' + sourceFolder;
	var fileList = getFileListingFromSource(sourceDirStr, filePattern);
	if(fileList != null && fileList.length > 0)
	{
		service.setOperation('cd',targetFolderStr);
		var result = service.call();
		if(!('OK'.equalsIgnoreCase(result.status)))
		{
			service.setOperation('mkdir',targetFolderStr);
			service.call();
			service.setOperation('cd',targetFolderStr);
			service.call();
		}
		for(var i = 0; i < fileList.length; i++)
		{
			var fileCopyResult = copyFileToTarget(service,targetFolderStr,fileList[i]);
			if (!fileCopyResult.done) {
				return {'done': false, 'message' : 'error while transfering file : '+fileCopyResult.message};
			}
		}
		return {'done': true, 'message' : ''};
	}
	return {'done': false, 'message' : 'File-List was empty.', 'noFilesFound': true};
}

/**
*	get list of files which matches the file pattern
**/
function getFileListingFromSource(sourceFolder , filePattern)
{
	var ArrayList = require('dw/util/ArrayList');
	var fileList = new ArrayList();
	var File = require('dw/io/File');
	var theDir = new File(File.IMPEX + sourceFolder);
	var regExp = new RegExp(filePattern);
	fileList.addAll(theDir.listFiles(function(file)
	{
		if (!empty(filePattern)) {
			return regExp.test(file.name);
		}
		return true;
    }));
	return fileList;
}
/**
*	Copy (and archive locally) a file to the remote FTP-Target-Folder
*
**/
function copyFileToTarget(service ,targetFolderStr, file )
{
    service.setOperation('putBinary',targetFolderStr+'/'+file.name,file);
	var result =service.call();
	if (!'OK'.equalsIgnoreCase(result.status)) {
		let msg = result.errorMessage || result.replyMessage || 'unknown error';
		return {'done' : false, 'message' : msg};
	}
	return {'done' : true, 'message' : ''};
}


exports.uploadFilesToSFTP = uploadFilesToSFTP;
