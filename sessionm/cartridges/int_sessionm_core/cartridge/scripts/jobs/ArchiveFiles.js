/* Copyright (C) 2017 SessionM. All Rights Reserved */

/* 
* archiveFiles .js  archive and delete files at IMPEX
*/


function archiveFilesInImpex(){
	try{
		var Logger = require('dw/system/Logger');
		var File = require('dw/io/File');
		var	sourceFolder = arguments[0].get('sourceFolder');
		var	archiveFolder = new File(File.IMPEX + sourceFolder+ File.SEPARATOR + 'archive');
		if (!archiveFolder.exists()) {
			archiveFolder.mkdirs();
		}
		var ComConst = require('../common/CommonConstants').CommonConstants;
		var helper = require(ComConst.PATH_HELPER+'SMHelper');
		var allFiles = new File(File.IMPEX + sourceFolder);
		var listOfAllFiles = allFiles.listFiles();
		for each(var file in listOfAllFiles){
			var copyFile = helper.copyFile(file, archiveFolder, sourceFolder);
			if(!copyFile.done ){
				Logger.error(copyFile.message);
				throw new Error(copyFile.message);
			}else{
				Logger.error(copyFile.message);
			}
		}
	}catch(e){
	   	  Logger.error('Error in archiving the file '+ e);
   }
   finally{
		return;
   }
}

module.exports.ArchiveFilesInImpex = archiveFilesInImpex;