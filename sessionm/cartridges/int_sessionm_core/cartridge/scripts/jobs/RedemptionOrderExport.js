/* Copyright (C) 2017 SessionM. All Rights Reserved*/

 /*
 * RedemptionOrderExport.js will contain the code to export orders to Session M,
 * for redemption for SessionM points.
 */

var orderItr,
	adjustment,
	CommonConstants = require('../common/CommonConstants').CommonConstants;;

function redemptionOrderExport() {
	try {
		orderItr = dw.order.OrderMgr.queryOrders('(status={0} OR status={1})  AND (custom.smOrderTobeRedeemed = {2} OR custom.smOrderTobeRedeemed = {3})', null , dw.order.Order.ORDER_STATUS_NEW , dw.order.Order.ORDER_STATUS_OPEN , null ,true);
		processOrders();
	} catch(exp){
		throw new Error(exp);
	}finally{
		return;	
	}
}

 /*
 * Process the orderItr and get the product which needs to be updated.
 */
function processOrders(){
	for each(var order in orderItr){
		adjustment = order.getPriceAdjustmentByPromotionID(CommonConstants.REWARD_POINTS);
		if(!empty(adjustment)){
			exportOrder(order);
		}
	}
}

 /*
 * Exports the order through the order redemption service
 */
function exportOrder(order){
	var	svc = require(CommonConstants.DW_SVC_PATH),
		adjustment = order.getPriceAdjustmentByPromotionID('rewards_points'),
		redemedQuanity = -1 * adjustment.basePrice.value * 100,
		service = svc.ServiceRegistry.get('int_sessionm_order'),
		orderInfo = {
						'order': {
							'quantity': redemedQuanity,
							'ip': order.remoteHost
						}
					 };
	var	requestObj = {
						orderInfo : JSON.stringify(orderInfo),
						customerNo : order.customer.profile.getCustomerNo()
					};
	var response = service.call(requestObj);
	//if the request has been successfully made then update the order status
	if ((!empty(response)) &&
		(CommonConstants.SERVICE_SUCCESS.equals( response.getStatus()))){
		updateOrderStatus(response , order);
	}
}

function updateOrderStatus(response , order){
	var responseObj = JSON.parse(response.object);
	if(CommonConstants.SERVICE_SUCCESS.equalsIgnoreCase( responseObj.status)){
		require(CommonConstants.DW_TRANSACTION_PATH).wrap(function(){
			//Update the customer and order with the response.
			var customerToUpdate = dw.customer.CustomerMgr.getCustomerByCustomerNumber(order.customer.profile.getCustomerNo());
	 		customerToUpdate.profile.custom.smAvailable_points = responseObj.order.user.available_points;
			order.custom.smPointRedemptionDone = true;
			order.custom.smOrderCustom2 = responseObj.order.id;
 		});
	}
}

module.exports.redemptionOrderExport = redemptionOrderExport;
