/* Copyright (C) 2017 SessionM. All Rights Reserved*/

/* 
* OrdersExport.ds will contain the code to export orders to Session M, either
* through HTTP request or by writing the orders xml to a file and exporting the file
* to SessionM's SFTP location.
*/
 var CommonConstants = require('../common/CommonConstants').CommonConstants,
	exportedOrdersList,
	fileWriter,
	orderItr;
 var OrderMgr = require('dw/order/OrderMgr');
/*
* The function called by the job to create file to export order through SFTP
*/
function smordersExportSFTP() {
	try {
		var index = 0,
			fileName = arguments[index].get('fileName'),
			filePath = arguments[index].get('filePath');
		orderItr =  OrderMgr.queryOrders('(status={0} OR status={1}) AND (custom.smExported = {2} OR custom.smExported = {3}) AND (custom.smOrderTobeRedeemed = {4} OR custom.smOrderTobeRedeemed = {5})', "orderNo ASC" , dw.order.Order.ORDER_STATUS_NEW , dw.order.Order.ORDER_STATUS_OPEN , null , false, null,false );
		if(orderItr.count !== 0){
			var ordersXml = getOrdersXml();
			if(!empty(fileName) && !empty(filePath)){
				createOrdersExportFile(fileName , filePath);
				fileWriter.write(ordersXml.toXMLString());
				fileWriter.close();
				updateSmOrderStatus();
			} else{
				throw new Error('fileName or filePath is empty.');
			}
		} else{
			dw.system.Logger.info('No orders to export');
		}
	} catch(exp) {
		dw.system.Logger.error('Export Orders job={0}',exp);
		throw new Error(exp);
	}finally{
		return;
	}
}

/*
* The function called by the job for exporting order through HTTP request
*/
function smordersExportHTTPS() {
	try {
		 orderItr =  OrderMgr.queryOrders('(status={0} OR status={1}) AND (custom.smExported = {2} OR custom.smExported = {3}) AND (custom.smOrderTobeRedeemed = {4} OR custom.smOrderTobeRedeemed = {5})',"orderNo ASC", dw.order.Order.ORDER_STATUS_NEW , dw.order.Order.ORDER_STATUS_OPEN ,null,false,null,false);
		if(orderItr.count !== 0){
			var ordersXml = getOrdersXml();
			processHttpRequest(ordersXml);
		} else{
			dw.system.Logger.info('No orders to export');
		}
	} catch(exp) {
		dw.system.Logger.error('Export Orders job={0}',exp);
		throw new Error(exp);
	}finally{
		return;
	}
}

/*
 * The function processes the orders found through the queryOrder and returns
 * the <orders> xml for the same.
 */
function getOrdersXml(){
	var ordersXml = <orders xmlns='http://www.demandware.com/xml/impex/order/2006-10-31'></orders>,
		ArrayList = require('dw/util').ArrayList,
		noOfOrdersAdded = 0,
		totalOrdersToBeExported = dw.system.Site.getCurrent().getCustomPreferenceValue('smNoOfOrderExported');
	exportedOrdersList = new ArrayList();
	for each(var order in orderItr) {
		exportedOrdersList.add(order);
		var orderXml = <order order-no={order.getOrderNo()}></order>;
		var creationDate = order.creationDate;
		var orderCreationDate = dw.util.StringUtils.formatCalendar(new dw.util.Calendar(creationDate), CommonConstants.EXPORT_ORDER_DATE_FORMAT);
		orderXml['order-date'] = orderCreationDate;
		orderXml['created-by'] = order.createdBy;
		orderXml['original-order-no'] = order.orderNo;
		orderXml['currency'] = order.currencyCode;
		orderXml['customer-locale'] = order.customerLocaleID;
		orderXml['taxation'] = dw.order.TaxMgr.getTaxationPolicy() === 1 ? CommonConstants.TAXATION_NET : CommonConstants.TAXATION_STANDARD;
		orderXml['invoice-no'] = order.invoiceNo;
		var customerXml = getCustomerDetails(order);
		orderXml.appendChild(customerXml);
		var orderStatusXML = getOrderStatus(order);
		orderXml.appendChild(orderStatusXML);
		orderXml['current-order-no'] = order.currentOrderNo;
		var productLineItemDetails = getProductLineItemDetails(order.getAllProductLineItems());
		orderXml.appendChild(productLineItemDetails);
		var shippmentLineItemDetails = getShippmentLineItemDetails(order.shipments);
		orderXml.appendChild(shippmentLineItemDetails);
		var shipmentDetails = getShipmentDetails(order.shipments);
		orderXml.appendChild(shipmentDetails);
		var orderTotals = getorderTotals(order);
		orderXml.appendChild(orderTotals);
		var paymentInstumentDetails = getPaymentIntrumentsDetails(order.paymentInstruments );
		orderXml.appendChild(paymentInstumentDetails);
		orderXml['remoteHost'] = order.remoteHost;
		orderXml.appendChild(getCustomAttributeDetails(order));
		ordersXml.appendChild(orderXml);
		if(++noOfOrdersAdded === totalOrdersToBeExported){
			break;
		}
	}
	return ordersXml;
}

/*
 * Provides the <customer> section XML for order export.
 */
function getCustomerDetails(order){
	var customerXml = new XML('<customer/>');
	customerXml['customer-no'] = order.getCustomerNo();
	customerXml['customer-name'] = order.customerName;
	customerXml['customer-email'] = order.customerEmail;
	var billingXml = new XML('<billing-address/>');
	var billingAddress = order.getBillingAddress();
	billingXml['first-name'] = billingAddress.firstName;
	billingXml['last-name'] = billingAddress.lastName;
	billingXml['address1'] = billingAddress.address1;
	if(!empty(billingAddress.address2)){
		billingXml['address2'] = billingAddress.address2;
	}
	billingXml['city'] = billingAddress.city;
	billingXml['postal-code'] = billingAddress.postalCode;
	billingXml['state-code'] = billingAddress.stateCode;
	billingXml['country-code'] = billingAddress.countryCode.value;
	billingXml['phone'] = billingAddress.phone;
	customerXml.appendChild(billingXml);
	return customerXml;
}

/*
 * Provides the <status> section XML for order export.
 */
function getOrderStatus(order) {
	var orderStatusXml = new XML('<status/>');
	orderStatusXml['order-status'] = order.getStatus().displayValue;
	orderStatusXml['shipping-status'] = order.shippingStatus.displayValue;
	orderStatusXml['confirmation-status'] = order.getConfirmationStatus().displayValue;
	orderStatusXml['payment-status'] = order.getPaymentStatus().displayValue;
	return orderStatusXml;
}

/*
 * Provides the <product-lineitems> section XML for order export.
 */
function getProductLineItemDetails(lineItems){
	var productLineItemsXml = new XML('<product-lineitems/>');
	for each(var lineItem in lineItems){
		if(lineItem.isOptionProductLineItem()){
			continue;
		}
		var productLineItemXml = new XML('<product-lineitem/>');
		productLineItemXml['net-price'] = lineItem.getNetPrice();
		productLineItemXml['tax'] = lineItem.getTax();
		productLineItemXml['gross-price'] = lineItem.getGrossPrice();
		productLineItemXml['base-price'] = lineItem.getBasePrice();
		productLineItemXml['lineitem-text'] = lineItem.getLineItemText();
		productLineItemXml['tax-basis'] =lineItem.getTaxBasis();
		productLineItemXml['position'] = lineItem.getPosition();
		productLineItemXml['product-id'] = lineItem.getProductID();
		productLineItemXml['product-name'] = lineItem.getProductName();
		productLineItemXml['quantity'] = lineItem.getQuantity();
		productLineItemXml['tax-rate'] = lineItem.getTaxRate();
		productLineItemXml['shipment-id'] = lineItem.shipment.shipmentNo;
		if(lineItem.getOptionProductLineItems().size() > 0){
			var optionProductLineItemDetails = getOptionProductLineItemsDetails(lineItem.getOptionProductLineItems());
			productLineItemsXml.appendChild(optionProductLineItemDetails);
		}
		if(lineItem.getPriceAdjustments().size() > 0){
			var priceAdjustmentDetails = getPriceAdjustmentDetails(lineItem.getPriceAdjustments());
			productLineItemXml.appendChild(priceAdjustmentDetails);
		}
		productLineItemXml['gift'] = lineItem.isGift();
		productLineItemsXml.appendChild(productLineItemXml);
	}
	return productLineItemsXml;
}

/*
 * Provides the <option-lineitems> section XML for order export.
 */
function getOptionProductLineItemsDetails(optionLineItems){
	var optionLineItemsXml = new XML('<option-lineitems/>');
	for each(var lineItem in optionLineItems){
		var optionLineItemXml = new XML('<option-lineitem/>');
		optionLineItemXml['net-price'] = lineItem.getNetPrice();
		optionLineItemXml['tax'] = lineItem.getTax();
		optionLineItemXml['gross-price'] = lineItem.getGrossPrice();
		optionLineItemXml['base-price'] = lineItem.getBasePrice();
		optionLineItemXml['lineitem-text'] = lineItem.getLineItemText();
		optionLineItemXml['tax-basis'] =lineItem.getTaxBasis();
		optionLineItemXml['option-id'] = lineItem.getOptionID();
		optionLineItemXml['value-id'] = lineItem.getOptionValueID();
		optionLineItemXml['product-id'] = lineItem.getProductID();
	}
	return optionLineItemsXml;
}

/*
 * Provides the <shipping-lineitems> section XML for order export.
 */
function getShippmentLineItemDetails(shipments){
	var shippingLineItemsXml = new XML('<shipping-lineitems/>');
	for each(var shipment in shipments){
		for each(var shippingLineItem in shipment.shippingLineItems){
			var shippingLineItemXml = new XML('<shipping-lineitem/>');
			shippingLineItemXml['net-price'] = shippingLineItem.netPrice.value;
			shippingLineItemXml['tax'] = shippingLineItem.tax.value;
			shippingLineItemXml['gross-price'] = shippingLineItem.grossPrice.value;
			shippingLineItemXml['base-price'] = shippingLineItem.basePrice.value;
			shippingLineItemXml['lineitem-text'] = shippingLineItem.lineItemText;
			shippingLineItemXml['tax-basis'] = shippingLineItem.taxBasis.value;
			shippingLineItemXml['item-id'] = shippingLineItem.ID;
			shippingLineItemXml['shipment-id'] = shipment.shipmentNo;
			shippingLineItemXml['tax-rate'] = shippingLineItem.taxRate;
			if(shippingLineItem.getShippingPriceAdjustments().size() > 0){
				var priceAdjustmentDetails = getPriceAdjustmentDetails(shippingLineItem.getShippingPriceAdjustments());
				shippingLineItemXml.appendChild(priceAdjustmentDetails);
			}
			shippingLineItemsXml.appendChild(shippingLineItemXml);
		}
	}
	return shippingLineItemsXml;
}

/*
 * Provides the <shipments> section XML for order export.
 */
function getShipmentDetails(shipments){
	var shipmentsXml = new XML('<shipments/>');
	for each(var shipment in shipments){
		var shipmentXml =  <shipment shipment-id={shipment.shipmentNo}></shipment>;
		var statusXml = <status>
				 			<shipping-status>{shipment.shippingStatus.displayValue}</shipping-status>
						</status>;
		shipmentXml.appendChild(statusXml);
		shipmentXml['shipping-method'] = shipment.shippingMethodID;
		var shippingAddress = shipment.shippingAddress;
		var shippingAddressXml = new XML('<shipping-address/>');
		shippingAddressXml['first-name'] = shippingAddress.firstName;
		shippingAddressXml['last-name'] = shippingAddress.lastName;
		shippingAddressXml['address1'] = shippingAddress.address1;
		if(!empty(shippingAddress.address2)){
			shippingAddressXml['address2'] = shippingAddress.address2;
		}
		shippingAddressXml['city'] = shippingAddress.city;
		shippingAddressXml['postal-code'] = shippingAddress.postalCode;
		shippingAddressXml['state-code'] = shippingAddress.stateCode;
		shippingAddressXml['country-code'] = shippingAddress.countryCode.value;
		shippingAddressXml['phone'] = shippingAddress.phone;
		shipmentXml.appendChild(shippingAddressXml);
		shipmentXml['gift'] = shipment.gift;
		var totalXml = new XML('<totals/>');
		var merchandizeTotalXML = <merchandize-total>
							<net-price>{shipment.merchandizeTotalNetPrice.value}</net-price>
							<tax>{shipment.merchandizeTotalTax.value}</tax>
							<gross-price>{shipment.merchandizeTotalGrossPrice.value}</gross-price>
						</merchandize-total>;
		totalXml.appendChild(merchandizeTotalXML);
		var adjustedMerchandizeTotalXML = <adjusted-merchandize-total>
							<net-price>{shipment.adjustedMerchandizeTotalNetPrice.value}</net-price>
							<tax>{shipment.merchandizeTotalTax.value}</tax>
							<gross-price>{shipment.merchandizeTotalGrossPrice.value}</gross-price>
						</adjusted-merchandize-total>;
		totalXml.appendChild(adjustedMerchandizeTotalXML);
		var shippingTotalXML = <shipping-total>
							<net-price>{shipment.shippingTotalNetPrice.value}</net-price>
							<tax>{shipment.shippingTotalTax.value}</tax>
							<gross-price>{shipment.shippingTotalGrossPrice.value}</gross-price>
						</shipping-total>;
		totalXml.appendChild(shippingTotalXML);
		var adjustedShippingTotalXML = <adjusted-shipping-total>
							<net-price>{shipment.adjustedShippingTotalNetPrice.value}</net-price>
							<tax>{shipment.adjustedShippingTotalTax.value}</tax>
							<gross-price>{shipment.adjustedShippingTotalGrossPrice.value}</gross-price>
						</adjusted-shipping-total>;
		totalXml.appendChild(adjustedShippingTotalXML);
		var shipmentTotalXML = <shipment-total>
							<net-price>{shipment.totalNetPrice.value}</net-price>
							<tax>{shipment.totalTax.value}</tax>
							<gross-price>{shipment.totalGrossPrice.value}</gross-price>
						</shipment-total>;
		totalXml.appendChild(shipmentTotalXML);
		shipmentXml.appendChild(totalXml);
		shipmentsXml.appendChild(shipmentXml);
	}
	return shipmentsXml;
}

/*
 * Provides the <price-adjustments> section XML for order export.
 */
function getPriceAdjustmentDetails(priceAdjustments){
	var priceAdjustmentsXml = new XML('<price-adjustments/>');
	for each(var priceAdjustment in priceAdjustments){
		var priceAdjustmentXml = new XML('<price-adjustment/>');
		priceAdjustmentXml['net-price'] = priceAdjustment.netPrice.value;
		priceAdjustmentXml['tax'] = priceAdjustment.tax.value;
		priceAdjustmentXml['gross-price'] = priceAdjustment.grossPrice.value;
		priceAdjustmentXml['base-price'] = priceAdjustment.basePrice.value;
		priceAdjustmentXml['lineitem-text'] = priceAdjustment.lineItemText;
		priceAdjustmentXml['tax-basis'] = priceAdjustment.taxBasis.value;
		if(!empty(priceAdjustment.promotionID)){
			priceAdjustmentXml['promotion-id'] = priceAdjustment.promotionID;
		}
		if(!empty(priceAdjustment.campaignID)){
			priceAdjustmentXml['campaign-id'] = priceAdjustment.campaignID;
		}
		if(!empty(priceAdjustment.couponLineItem)){
			priceAdjustmentXml['coupon-id'] = priceAdjustment.couponLineItem.couponCode;
		}
		priceAdjustmentsXml.appendChild(priceAdjustmentXml);
	}
	return priceAdjustmentsXml;
}

/*
 * Provides the <totals> section XML for order export.
 */
function getorderTotals(order){
	var totalXml = new XML('<totals/>');
	var merchandizeTotalXML = new XML('<merchandize-total/>');
	merchandizeTotalXML['net-price'] = order.merchandizeTotalNetPrice.value;
	merchandizeTotalXML['tax'] = order.merchandizeTotalTax.value;
	merchandizeTotalXML['gross-price'] = order.merchandizeTotalGrossPrice.value;
	if(order.getPriceAdjustments().size() > 0){
		var priceAdjustmentDetails = getPriceAdjustmentDetails(order.getPriceAdjustments());
		merchandizeTotalXML.appendChild(priceAdjustmentDetails);
	}
	totalXml.appendChild(merchandizeTotalXML);
	var adjustedMerchandizeTotalXML = <adjusted-merchandize-total>
						<net-price>{order.adjustedMerchandizeTotalNetPrice.value}</net-price>
						<tax>{order.merchandizeTotalTax.value}</tax>
						<gross-price>{order.merchandizeTotalGrossPrice.value}</gross-price>
					</adjusted-merchandize-total>;
	totalXml.appendChild(adjustedMerchandizeTotalXML);
	var shippingTotalXML = <shipping-total>
						<net-price>{order.shippingTotalNetPrice.value}</net-price>
						<tax>{order.shippingTotalTax.value}</tax>
						<gross-price>{order.shippingTotalGrossPrice.value}</gross-price>
					</shipping-total>;
	totalXml.appendChild(shippingTotalXML);
	var adjustedShippingTotalXML = <adjusted-shipping-total>
						<net-price>{order.adjustedShippingTotalNetPrice.value}</net-price>
						<tax>{order.adjustedShippingTotalTax.value}</tax>
						<gross-price>{order.adjustedShippingTotalGrossPrice.value}</gross-price>
					</adjusted-shipping-total>;
	totalXml.appendChild(adjustedShippingTotalXML);
	var orderTotalXML = <order-total>
						<net-price>{order.totalNetPrice.value}</net-price>
						<tax>{order.totalTax.value}</tax>
						<gross-price>{order.totalGrossPrice.value}</gross-price>
					</order-total>;
	totalXml.appendChild(orderTotalXML);
	return totalXml;
}

/*
 * Provides the <Payments> section XML for order export.
 */
function getPaymentIntrumentsDetails(paymentInstruments){
	var paymentsXml = new XML('<payments/>');
	for each(var paymentInstrument in paymentInstruments){
		var paymentMethod = paymentInstrument.paymentMethod,
			creditCardNo;
		if (paymentMethod.equals('CREDIT_CARD')){
			if(dw.system.Site.getCurrent().getCustomPreferenceValue('smCcNoExportType').value.equals(CommonConstants.MASKED)){
				creditCardNo = paymentInstrument.maskedCreditCardNumber;
			}else if(dw.system.Site.getCurrent().getCustomPreferenceValue('smCcNoExportType').value.equals(CommonConstants.ENCRYPTED)){
				var publicKey = dw.system.Site.getCurrent().getCustomPreferenceValue('smCcPublickey');
				creditCardNo = paymentInstrument.getEncryptedCreditCardNumber(paymentInstrument.ENCRYPTION_ALGORITHM_RSA,publicKey);
			}
			var cardToken  = !empty(paymentInstrument.creditCardToken) ? paymentInstrument.creditCardToken :'';
			var creditCardPaymentXML =
				<payment>
					<credit-card>
	                    <card-type>{paymentInstrument.creditCardType}</card-type>
	                    <card-number>{creditCardNo}</card-number>
	                    <card-token>{cardToken}</card-token>
	                    <card-holder>{paymentInstrument.creditCardHolder}</card-holder>
	                    <expiration-month>{paymentInstrument.creditCardExpirationMonth}</expiration-month>
	                    <expiration-year>{paymentInstrument.creditCardExpirationYear}</expiration-year>
                	</credit-card>
                	<processor-id>{paymentInstrument.paymentTransaction.paymentProcessor.ID}</processor-id>
                	<amount>{paymentInstrument.paymentTransaction.amount.value}</amount>
                	<transaction-id>{paymentInstrument.paymentTransaction.transactionID}</transaction-id>
        		</payment>;
        		paymentsXml.appendChild(creditCardPaymentXML);
		} else {
			var customPaymentXML =
				<payment>
					<custom-method>
	                    <method-name>{paymentInstrument.paymentMethod}</method-name>
                	</custom-method>
                	<amount>{paymentInstrument.paymentTransaction.amount.value}</amount>
                	<transaction-id>{paymentInstrument.paymentTransaction.transactionID}</transaction-id>
        		</payment>;
        		paymentsXml.appendChild(customPaymentXML);
		}
	}
 return paymentsXml;
}

/*
 * Provides the <custom-attributes> section XML for order export.
 */
function getCustomAttributeDetails(order){
		var customAttributesXml = new XML('<custom-attributes/>'),
			orderCustomArr = CommonConstants.ORDER_CUSTOM_ATTRIBUTES;
	for(var i=0 ; i<orderCustomArr.length ; i++) {
  		var attribute = orderCustomArr[i];
  		if(attribute in order.custom){
  			var customAttrXml =  <custom-attribute attribute-id={attribute}>{order.custom[attribute]}</custom-attribute>;
			customAttributesXml.appendChild(customAttrXml);
  		}
	}
	return customAttributesXml;
}

/*
 * If the orders are to be exported through HTTP service then the
 * following function is executed.
 */
function processHttpRequest(ordersXml){
	// Load the service configuration
	var service =  require('dw/svc').ServiceRegistry.get(CommonConstants.EXPORT_ORDER_SERVICE_ID);
	//Call the Add Address WebService
	var response = service.call( ordersXml );
	if( (!empty(response)) &&
			(CommonConstants.SERVICE_SUCCESS.equals( response.getStatus() ))){
		updateSmOrderStatus();
		return;
	} else {
		dw.system.Logger.error('SessionM Order Export job={0}',response.getStatus());
		throw new Error(response.getStatus());
	}
}

/*
 * Creates the order export file in the IMPEX and with the timestamp added
 * to the file name.
 */
function createOrdersExportFile ( fileName, fileLocation ) {
	fileName = require('../pipelets/AppendTimeStamp').AppendTimestampToFileName(fileName);
	var File = require('dw/io').File,
		FileWriter = require('dw/io').FileWriter,
	 	orderExportFile = new File( 'IMPEX/src/' + fileLocation + '/' + fileName );
	if( orderExportFile.exists() ) {
		orderExportFile.remove();
	}
	orderExportFile.createNewFile();
	fileWriter = new FileWriter( orderExportFile, CommonConstants.ENCODING_FORMAT );
}

 /*
 * Updates the custom SM status variable after the orders have been
 * successfully exported
 */
function updateSmOrderStatus(){
	for each(var order in exportedOrdersList){
		order.custom.smExported = true;
	}
}

exports.smordersExportSFTP = smordersExportSFTP;
exports.smordersExportHTTPS = smordersExportHTTPS;