/* Copyright (C) 2017 SessionM. All Rights Reserved*/

/*
 * Component file to download the files  fro SFTP
 */
var	CommonConstants = require('../common/CommonConstants').CommonConstants,
 	Logger = require(CommonConstants.REQUIRE_LOGGER),
	ArrayList = require(CommonConstants.REQUIRE_ARRAYLIST),
	MEGABYTE = CommonConstants.MEGABYTE_VALUE, // 1024 * 1024
	FILE_SIZE_DOWNLOAD_LIMIT = 200 * MEGABYTE,
	File = require(CommonConstants.REQUIRE_FILE);



function downloadFilesFromSFTP(){
	try{
		var index = 0;
		var filePattern = arguments[index].get(CommonConstants.FILE_PATTERN);
		var folderSource = arguments[index].get(CommonConstants.SOURCE_FOLDER);
		var folderTarget = arguments[index].get(CommonConstants.TARGET_FOLDER);
		var isDelete = false;
		var copyResult = 0;
		var errorMsg = '';
		var svc  = require('dw/svc');
		var service  =  svc.ServiceRegistry.get('int_sessionm_sftpservice');
		var configuration =	service.getConfiguration();
		var credentials = configuration.getCredential();
		var user = credentials.getUser();
		var password= credentials.getPassword();
		var url = credentials.getURL();
		if(empty(url) && empty(user) && empty(password) &&  empty(folderSource) && empty(folderTarget))
		{
			Logger.error( CommonConstants.PARAMETERS_MISSING);
			throw new Error(errorMsg);
		}
		service.setOperation('connect',url,user,password);
		result = service.call();
		//Copy (and archive) files
		if('OK'.equalsIgnoreCase(result.status)){
		  copyResult = copyFilesToTarget(service,folderTarget,isDelete,filePattern,folderSource);
		 }else{
	      errorMsg = CommonConstants.CONNECTION_UNSUCCESSFUL;
		  Logger.error( errorMsg);
		  throw new Error(errorMsg);
		}
		if(!copyResult)
		{
			Logger.error(CommonConstants.FILE_LIST_EMPTY);
			throw new Error(CommonConstants.FILE_LIST_EMPTY);
		}
	}catch(e){
		Logger.error('Error while executing method downloadFilesFromSFTP in file DownloadFilesFromSFTP.ds' +e);
	}finally{
		return;
	}
}

	
/**
*	Copy (and delete) files from a remote FTP-Folder locally
*	param ftpClient 	: Object 	FTP Client used
*	param sourceFolder : String 	source Folder
*	param filePattern 	: String 	The pattern for the filenames
*	param targetFolder : String 	target FTP Folder
*	param deleteFile 	: Boolean 	Flag if files should be deleted after successful copying
*	
*	returns Boolean If files were found at the specified location.
**/
function copyFilesToTarget( svc,folderTarget,isDelete,filePattern,folderSource)
{
	svc.setOperation('cd',folderSource);
	var result = svc.call();
	var regExp : RegExp = new RegExp(filePattern);
	var isCopied = false;
	if(('OK'.equalsIgnoreCase(result.status))){
		svc.setOperation('list',folderSource);
		var listStatus = svc.call();
		if('ERROR'.equalsIgnoreCase(listStatus.status)){
			throw new Error(CONNECTION_UNSUCCESSFUL);
		}
		var fileInfoList = listStatus.object;
		if(fileInfoList != null && fileInfoList.length > 0)
		{
			for(var i : Number = 0; i < fileInfoList.length; i++)
			{
				var fileInfo = fileInfoList[i];
			    var fileName = fileInfo.name;
			    if(regExp.test(fileName)){
			    	var fileSize = fileInfo.size;
			    	if ( fileSize > FILE_SIZE_DOWNLOAD_LIMIT ) {
			    		throw new Error( 'FTP download file size limit of 200 MB exceeded for ' + fileInfo.name + '. Actual size is ' + (fileSize/MEGABYTE).toFixed(2) + ' MB.');
			    	}else {
					    copyFileToTargetFolder(svc, folderTarget, folderSource,fileName, isDelete);
					    isCopied =true;
					}
			    }
			}
		}
	}
  return isCopied;
}


/**
*	Copy (and delete) a file from a remote FTP-Folder locally
*	param ftpClient 	: Object 	FTP Client used
*	param targetFolder : String 	target FTP Folder
*	param fileName 	: String 	The file to copy
*	param deleteFile 	: Boolean	Flag if files should be deleted after successful copying
*
**/
function copyFileToTargetFolder(svc, folderTarget,folderSource, fileName, isDelete)
{
	var targetDirStr = folderTarget.charAt(0).equals('/') ? folderTarget  : '/' + folderTarget;
	var theDir = new File(CommonConstants.IMPEX + targetDirStr);
	theDir.mkdirs();
	var theFile = new File(theDir.fullPath + '/' + fileName);
	var result = svc.setOperation('getBinary',folderSource+'/'+fileName,theFile);
	result = svc.call();
	theFile.createNewFile();
	if(isDelete)
	{
		svc.setOperation('del',fileName);
		svc.call();
	}
}


module.exports = {
	'DownloadFilesFromSFTP' : downloadFilesFromSFTP
};