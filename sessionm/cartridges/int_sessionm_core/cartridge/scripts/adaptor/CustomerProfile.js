/* Copyright (C) 2017 SessionM. All Rights Reserved */

/*
* Initiate the customer registration with SessionM
* param - customer DTP
*/
function registration(customerProfile ){
	var result ={};
	var ComConst = require('../common/CommonConstants').CommonConstants;
	var Site = require(ComConst.REQUIRE_SITE);
	var Logger = require(ComConst.REQUIRE_LOGGER);
    // If Site Preference is disabled return empty response
	if(!Site.getCurrent().getCustomPreferenceValue('IsSMEnabled')){
      return result;
	}
	try{
		var customerFacade = require(ComConst.PATH_FACADE+'CustomerProfileFacade');
		var helper = require(ComConst.PATH_HELPER+'SMHelper');
		result  =	customerFacade.CreateCustomer( customerProfile );
		if(!empty(result.responseObj) && 'ok'.equalsIgnoreCase(result.responseObj.status)){
    		helper.updateSMCustomerProfileFields(customerProfile,result.responseObj.user);
		}
	}catch(e){
		Logger.error('Error occured while Registering the customer '+ e);
	}finally{
		return result;
	}
 }; 
 /*
 * Initiate the customer profile fetch with SessionM
 * param - customer DTP
 */
 function retrieveCustomer(customerProfile) {
 	var response;
 	var ComConst = require('../common/CommonConstants').CommonConstants;
 	var Site = require(ComConst.REQUIRE_SITE);
 	var Logger = require(ComConst.REQUIRE_LOGGER);
 	// If Site Preference is disabled return empty response
 	if(!Site.getCurrent().getCustomPreferenceValue('IsSMEnabled')){
 	   return ;
 	}
 	try{
 		var customerFacade = require(ComConst.PATH_FACADE+'CustomerProfileFacade');
 		var helper = require(ComConst.PATH_HELPER+'SMHelper');
 		var smUUId = customerProfile.custom.smUUID;
 		if(null !== smUUId || !empty(smUUId)){
 			response = customerFacade.RetrieveCustomer(customerProfile);
 			/*Sync customer profile and address details*/
 			if(!empty(response.responseObj) && 'ok'.equalsIgnoreCase(response.responseObj.status)){
 				helper.updateSMCustomerProfileFields(customerProfile,response.responseObj.user);
 				helper.updateCustomerProfile(response.responseObj.user, customerProfile);
 			}
 		}
 		else{
 			response = customerFacade.SearchCustomer(customerProfile);
 			/*update above details  for seach and then make call to update profile*/
 			if(!empty(response.responseObj) && 'ok'.equalsIgnoreCase(response.responseObj.status)){
 				helper.updateSMCustomerProfileFields(customerProfile,response.responseObj.user);
 				helper.updateCustomerProfile(response.responseObj.user, customerProfile  );
 				var updateCustomerFacade = require(ComConst.UPDATE_CUSTOMER);
 				updateCustomerFacade.UpdateCustomer(customerProfile);
 			}
 		}
 	}catch(e){
 			Logger.error('[CustomerProfile.js] Error after Sign in Fetch/Search SM ( {0} )',e.message);
 	}finally{
 		return response;
 	}
 }
 /*
 * Initiate the customer profile data updation with SessionM
 * param - customerProfile, customerAddress
 */

 function updateCustomer(customerProfile){
 	var result ={};
 	var ComConst = require('../common/CommonConstants').CommonConstants;
 	var Site = require(ComConst.REQUIRE_SITE);
 	var Logger = require(ComConst.REQUIRE_LOGGER);
 	if(!Site.getCurrent().getCustomPreferenceValue('IsSMEnabled')){
     	return result;
 	}
 	try{
 		var customerFacade = require(ComConst.PATH_FACADE+'CustomerProfileFacade');
 		var helper = require(ComConst.PATH_HELPER+'SMHelper');
     	result  = customerFacade.UpdateCustomer(customerProfile);
    		if(!empty(result.responseObj) && 'ok'.equalsIgnoreCase(result.responseObj.status)){
     		helper.updateSMCustomerProfileFields(customerProfile,result.responseObj.user);
    		 }
 		}catch(e){
 			Logger.error('Error occured while Registering the customer '+ e);
 		}finally{
 			return result;
 		}
 }
 
 function handleErrorCodeForRedemptionResponse(customer){
		var ComConst = require('../common/CommonConstants').CommonConstants;
		let r = require(ComConst.SG_CONTROLLER+'/cartridge/scripts/util/Response');
		var CustAdaptor = require(ComConst.PATH_ADAPTOR+'/CustomerProfile');
		var result = CustAdaptor.RetrieveCustomer(customer.profile);
		return result;
	}
	
	/*
  * Initiate the customer reward points update SessionM
  */

function updateCustomerRewardPoints(customerProfile) {
   	var response;
   	var ComConst = require('../common/CommonConstants').CommonConstants;
	var Site = require(ComConst.REQUIRE_SITE);
	var Logger = require(ComConst.REQUIRE_LOGGER);
	// If Site Preference is disabled return empty response
	if(!Site.getCurrent().getCustomPreferenceValue('IsSMEnabled')){
	   return response;
	}
	try{
		var customerFacade = require(ComConst.PATH_FACADE+'CustomerProfileFacade');
		var helper = require(ComConst.PATH_HELPER+'SMHelper');
		var smUUId = customerProfile.custom.smUUID;
		if(null !== smUUId || !empty(smUUId)){
			response = customerFacade.RetrieveCustomer(customerProfile);
			/*Sync customer profile and address details*/
			if(!empty(response.responseObj) && 'ok'.equalsIgnoreCase(response.responseObj.status)){
				helper.updateCustomerRewardPoints(customerProfile,response.responseObj.user);
			}
		}
	}catch(exp){
		Logger.error('[CustomerProfile.js] Error synchronizing SM Rewards Points ( {0} )',exp);
   	}finally{
   		if(!empty(response) && !empty(response.responseObj) && 'ok'.equalsIgnoreCase(response.responseObj.status)){
   			return true;
   		} else {
   			return false;
   		}
   	}
}

module.exports = {
      'Registration': registration,
      'RetrieveCustomer': retrieveCustomer,
      'UpdateCustomer': updateCustomer,
      'HandleErrorCodeForRedemptionResponse': handleErrorCodeForRedemptionResponse,
      'UpdateCustomerRewardsPoints' : updateCustomerRewardPoints
};