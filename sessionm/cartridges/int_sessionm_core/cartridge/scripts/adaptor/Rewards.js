/* Copyright (C) 2017 SessionM. All Rights Reserved */

/*
* Perform service call for Redeeming SessionM points
* param - order
*/

var ComConst = require('../common/CommonConstants').CommonConstants;

function exportRewardRedemption(order){
	var adjustment = order.getPriceAdjustmentByPromotionID('rewards_points');
	if(!empty(adjustment)){
		require(ComConst.DW_TRANSACTION_PATH).wrap(function(){
	 		order.custom.smOrderTobeRedeemed = true;
 		});
        var RewardsFacade = require(ComConst.PATH_FACADE+'RewardsFacade');
		return RewardsFacade.RedeemSessionmPoints(order);
	}
	return true;
}

function calculateReward(cart,redeemableAmount){
	var result = {};
	if (cart.getTotalGrossPrice() > 0) {
            result = {
                error: true
        };
     }
    var limitPercentage = dw.system.Site.getCurrent().getCustomPreferenceValue('smOrderRedemetionLimit').value,
    	redeemableLimit = (cart.getTotalGrossPrice().value/100) * limitPercentage,
 		Transaction = require(ComConst.DW_TRANSACTION_PATH);
 	if(redeemableAmount  > redeemableLimit){
 	    result = {
                error: true
        };
 	    return result;
 	}
	var amountDiscount  = new dw.campaign.AmountDiscount(redeemableAmount),
		adjustment = cart.getPriceAdjustmentByPromotionID('rewards_points');
	if(empty(adjustment)){
		Transaction.wrap(function(){
		 var priceAdj=	cart.createPriceAdjustment('rewards_points',amountDiscount);
		     priceAdj.lineItemText=ComConst.REWARD_PTSTEXT;
		});
		result = {success : true};
	} else {
		result = { error: true };
	}
   return result;
}

function removeRewards(cart){
	var result = {sucess : false};
	var Transaction = require(ComConst.DW_TRANSACTION_PATH);
	var adjustment = cart.getPriceAdjustmentByPromotionID('rewards_points');
	if(!empty(adjustment)){
		Transaction.wrap(function(){
			cart.removePriceAdjustment(adjustment);
		});
		result = {success : true};
	}
   return result;
}

function updateCartRewardPoints(cart){
	var adjustment = cart.getPriceAdjustmentByPromotionID('rewards_points');
	if(!empty(adjustment)){
		cart.removePriceAdjustment(adjustment);
	}
}
function cancelRedemptionOrder(order){
	var Transaction = require(ComConst.DW_TRANSACTION_PATH);
    return Transaction.wrap(function () {
    	var OrderMgr = require(ComConst.DW_ORDERMGR_PATH);
     	var app = require( ComConst.APP);
     	var Cart = app.getModel('Cart');
    	OrderMgr.failOrder(order);
    	var cart = Cart.get();
    	var adjustment = cart.getPriceAdjustmentByPromotionID('rewards_points');
		cart.removePriceAdjustment(adjustment);
	    return {
	      error: true
	    };
	});
}


module.exports = {
      'CalculateReward': calculateReward,
      'RemoveRewards' : removeRewards,
      'ExportRewardRedemption' : exportRewardRedemption,
      'UpdateCartRewardPoints' :  updateCartRewardPoints,
      'CancelRedemptionOrder' :  cancelRedemptionOrder
};