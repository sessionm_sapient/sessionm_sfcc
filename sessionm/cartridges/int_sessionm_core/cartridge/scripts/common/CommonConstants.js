/* Copyright (C) 2017 SessionM. All Rights Reserved */
/*
*@constructs CommonConstants
*/
function commonConstants() {
}
/*SFCC file constants*/
commonConstants.XML_FILE_EXT = '.xml';
commonConstants.DATE_PATTERN = 'MMddyyyyHHmmss';
commonConstants.XML_FILE_PATTERN = '^[\\w\-]{1,}\\.xml$';
commonConstants.USER_ID = 'userID' ;
commonConstants.IMPEX = 'IMPEX';
commonConstants.PASSWORD = 'password';
commonConstants.SOURCE_FOLDER = 'sourceFolder';
commonConstants.TARGET_FOLDER = 'targetFolder';
commonConstants.SERVER_TYPE = 'serverType';
commonConstants.FILE_PATTERN = 'filePattern';
commonConstants.SFTP = 'SFTP' ;
commonConstants.MEGABYTE_VALUE = 1048576;
commonConstants.SERVER_URL = 'serverUrl' ;
/*Session M service constants*/
commonConstants.GET = 'GET';
commonConstants.PUT = 'PUT';
commonConstants.AUTHORIZATION = 'Authorization';
commonConstants.BASIC = 'BASIC ';
commonConstants.CONTENT_TYPE_HEADER = 'Content-Type';
commonConstants.APPLICATION_JSON = 'application/json';
commonConstants.USER = 'user';
commonConstants.PASSWORD = 'password';
commonConstants.SERVICE_SUCCESS = 'OK';
commonConstants.GET_PRFL_SRV_ID = 'int_sessionm_retrieveprofile';
commonConstants.SRH_PRFL_SRV_ID = 'int_sessionm_searchprofile';
commonConstants.FTH_MULTI_PRFL_SRV_ID = 'int_sessionm_retrievemultipleprofile';
commonConstants.CREATE_PROFILE_SERVICE_ID = 'int_sessionm_createprofile';
commonConstants.UPDATE_PROFILE_SERVICE_ID = 'int_sessionm_updateprofile';
commonConstants.EXPORT_ORDER_SERVICE_ID = 'int_sessionm_exportorder';
commonConstants.TIER_SRV_ID = 'int_sessionm_tierdetail';
/*SessionM attribute mapping Arrays*/
var addressArr = ['ID','address1','address2','city','companyName','countryCode','firstName','jobTitle','lastName','phone','postBox','postalCode','salutation','secondName','stateCode','suffix','suite','title'];
commonConstants.ADDRESSARRAY = addressArr;
var profileArr = ['companyName','fax','firstName','lastName','gender','phoneBusiness','phoneHome','phoneMobile','salutation','secondName','suffix','title','jobTitle','email','salutation'];
commonConstants.PROFILEARRAY = profileArr;
var smCustomAttrMap = [ {key:'smUUID' , value:'id'}, {key:'smProxy_ids',value:'proxy_ids'},{key:'smAccountStatus' , value:'account_status'},{key:'smSuspended' , value:'suspended'},{key:'smAvailable_points',value:'available_points'},{key:'smAuthToken' , value:'auth_token'},{key:'smMember_Since',value:'created_at'}];
commonConstants.SFCC_CUSTOM_ATTRIBUTESMAP = smCustomAttrMap;
var smTierCustomAttrMap = [ {key:'smId' , value:'id'}, {key:'smIdentifier',value:'identifier'},{key:'smMaintenanceRequiredPoints' , value:'maintenance_required_points'},{key:'smMultiplier' , value:'multiplier'},{key:'smName',value:'name'},{key:'smNextTierId' , value:'next_tier_id'},{key:'smNextTierId' , value:'next_tier_id'},{key:'smRequiredPoints' , value:'required_sponsor_points'},{key:'smRewardSystemId' , value:'rewards_system_id'},{key:'smTierSystemId' , value:'tier_system_id'}];
commonConstants.SFCC_TIER_MAP = smTierCustomAttrMap;
//Cartridge Names
commonConstants.SG_CORE = 'app_sitegenesis_core';
commonConstants.SG_PIPELINE = 'app_sitegenesis_pipelines';
commonConstants.SG_CONTROLLER = 'app_sitegenesis_controllers';
//SM Cartridge Names
commonConstants.SM_CONTROLLER = 'int_sessionm_controllers';
commonConstants.SM_CORE = 'int_sessionm_core';
commonConstants.SM_PIPELINE = 'int_sessionm_pipelines';

//Folder Paths
commonConstants.PATH_PIPELET = commonConstants.SM_CORE+'/cartridge/scripts/pipelets/';
commonConstants.PATH_ADAPTOR = commonConstants.SM_CORE+'/cartridge/scripts/adaptor/';
commonConstants.PATH_COMMON = commonConstants.SM_CORE+'/cartridge/scripts/common/';
commonConstants.PATH_FACADE = commonConstants.SM_CORE+'/cartridge/scripts/facade/';
commonConstants.PATH_JOBS = commonConstants.SM_CORE+'/cartridge/scripts/jobs/';
commonConstants.PATH_HELPER = commonConstants.SM_CORE+'/cartridge/scripts/helper/';


commonConstants.REQUIRE_REGISTER = commonConstants.SM_CORE+'/cartridge/scripts/pipelets/RegisterCustomer';
commonConstants.REQUIRE_EMAIL = commonConstants.SG_CONTROLLER+'/cartridge/scripts/models/EmailModel';
commonConstants.REQUIRE_RETRIEVE = commonConstants.SM_CORE+'/cartridge/scripts/pipelets/RetrieveCustomer';
commonConstants.REQUIRE_UPDATE = commonConstants.SM_CORE+'/cartridge/scripts/pipelets/UpdateCustomer';
commonConstants.REWARD_POINTS_PATH = commonConstants.SM_CORE+'/cartridge/scripts/pipelets/RewardsPoints';



/*include cartridge and require file paths*/
commonConstants.CUSTOMER_PROFILE_FACADE = '~/cartridge/scripts/facade/CustomerProfileFacade';
commonConstants.SMHELPER ='~/cartridge/scripts/helper/SMHelper';
commonConstants.UPDATE_CUSTOMER = '~/cartridge/scripts/pipelets/UpdateCustomer';

commonConstants.APP = commonConstants.SG_CONTROLLER+'/cartridge/scripts/app';
commonConstants.GUARD = commonConstants.SG_CONTROLLER+'/cartridge/scripts/guard';






commonConstants.DW_TRANSACTION_PATH = 'dw/system/Transaction';
commonConstants.DW_ORDERMGR_PATH = 'dw/order/OrderMgr';
commonConstants.REMOTE_SERVER_ERROR='Unknown remote server error.';
commonConstants.DW_SVC_PATH='dw/svc';
commonConstants.REQUIRE_ARRAYLIST = 'dw/util/ArrayList';
commonConstants.REQUIRE_FILE = 'dw/io/File';
commonConstants.REQUIRE_LOGGER = 'dw/system/Logger';
commonConstants.REQUIRE_SITE = 'dw/system/Site';
commonConstants.REQUIRE_SFTPClient = 'dw/net/SFTPClient';
commonConstants.REQUIRE_SFTPFileInfo = 'dw/net/SFTPFileInfo';
commonConstants.REQUIRE_FTPClient = 'dw/net/FTPClient';
/* SFCC error messages */
commonConstants.USERNAME_PASS_MANDATORY_ERROR = 'User-ID and Password are manadatory for SFTP-Connection.';
commonConstants.CONNECTION_UNSUCCESSFUL = 'the connection couldnt be established.';
commonConstants.CONNECTION_SUCCESSFUL = 'the connection couldnt be established.';
commonConstants.PARAMETERS_MISSING = 'one or more mandatory parameters are missing.';
commonConstants.FILE_LIST_EMPTY = 'File-List was empty';
var orderCustomArr = ['smOrderCustom1','smOrderCustom2','smOrderCustom3','smOrderCustom4','smOrderCustom5'];
commonConstants.ORDER_CUSTOM_ATTRIBUTES = orderCustomArr;
commonConstants.EXPORT_ORDER_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
commonConstants.ENCODING_FORMAT = 'UTF-8';
commonConstants.UNDERSCORE_CHAR = '_';
commonConstants.TAXATION_NET = 'net';
commonConstants.TAXATION_STANDARD = 'standard';
commonConstants.HTTP = 'HTTP';
commonConstants.SFTP = 'SFTP';
commonConstants.MASKED = 'MASKED';
commonConstants.ENCRYPTED = 'ENCRYPTED';
commonConstants.USERPROFILE_GENDER = 'gender';
commonConstants.USERPROFILE_COUNTRYCODE = 'countryCode';
commonConstants.USERADDRESS_PREFERRED = 'preferred';
commonConstants.UPDATEPROFILE_NOTWORKING = 'Customer Update profile failed for ';
commonConstants.CREATEPROFILE_NOTWORKING = 'Customer Create profile failed for ';
commonConstants.REWARD_POINTS = 'rewards_points';
commonConstants.REWARD_PTSTEXT = 'Rewards points discount';
exports.CommonConstants = commonConstants;