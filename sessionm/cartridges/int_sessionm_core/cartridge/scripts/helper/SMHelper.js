/* Copyright (C) 2017 SessionM. All Rights Reserved */

/*
* Contains various helper methods for various operation
* 
*/
var Logger = require('dw/system/Logger');
var Transaction = require('dw/system/Transaction');
var CommonConstants = require('../common/CommonConstants').CommonConstants;
/*
*  customerProfileReqObject will create customer profile request object
*/
module.exports.customerProfileReqObject = function ( customerProfile ) {
	var reqJson = {};
	var user = {};
	var userProfile = {};
	user.email=customerProfile.email;
	user.external_id = 	customerProfile.customerNo;
	user.auth_token = true;
    var profileArr = CommonConstants.PROFILEARRAY;
	for(var j=0, leng=profileArr.length; j<leng; j++) {
  		var attr = profileArr[j];
  		if(attr in customerProfile){
  			if(attr.equalsIgnoreCase(CommonConstants.USERPROFILE_GENDER)){
				userProfile[profileArr[j]] = customerProfile[profileArr[j]].value;
			  }else{
				userProfile[profileArr[j]] = customerProfile[profileArr[j]];
			  }
		  }
	}
	var addressArr = CommonConstants.ADDRESSARRAY;
	var preferredAddress = customerProfile.addressBook.preferredAddress;
	var addresses = customerProfile.addressBook.addresses;
	var customerAddressArr = [];
	if(addresses.size()>0){
		for each(var customerAddress in addresses){
			var address = {};
			for(var i=0, len=addressArr.length; i<len; i++) {
  				var attribute = addressArr[i];
  				if(attribute in customerAddress){
					if(attribute.equalsIgnoreCase(CommonConstants.USERPROFILE_COUNTRYCODE)){
						address[addressArr[i]] = customerAddress[addressArr[i]].value.toUpperCase();
					}else{
						address[addressArr[i]] = customerAddress[addressArr[i]];
					}
				}
				if(preferredAddress.ID.equals(customerAddress.ID)){
  					address[CommonConstants.USERADDRESS_PREFERRED]=true;
  				}else{
  					address[CommonConstants.USERADDRESS_PREFERRED]=false;
  				}
  			}
		customerAddressArr.push(address);
		}
	}
	userProfile.addresses = customerAddressArr;
	user.user_profile = userProfile;
	reqJson.user = user;
	return JSON.stringify( reqJson );
};

module.exports.customerProfileResponseObject = function (serviceResponse){
	if ((!empty(serviceResponse)) &&
	 	('OK'.equals( serviceResponse.getStatus()))){
		var responseData = {} ;
		responseData = JSON.parse(serviceResponse.object);
		return responseData;
	 }else{
		Logger.error(' An Error Occured  in Customer profile service response ! {0}', (!empty(serviceResponse)) ? serviceResponse.getStatus() :serviceResponse);
	   return null;
  }
};
/*
*param SM Profile
*param SFCC customer profile
*returns update SessionM profile and address details in SFCC
*/
module.exports.updateCustomerProfile = function (smUser, customerProfile ){
	if(smUser !== null && smUser.id === customerProfile.custom.smUUID && smUser.user_profile !== null){
		setSMCustomerProfileAttribute(smUser, customerProfile);
		//if user's single or multiple address exist on SessionM
		if(smUser.user_profile.addresses.length > 0 ){
			var smAddresses : ArrayList = smUser.user_profile.addresses;
			for each(var address in smAddresses){
				if(!empty(address)){
					var customerAddress = customerProfile.addressBook.getAddress(address.ID);
					if(customerAddress !== null ){
						setSMCustomerAddressAttribute(address, customerAddress, customerProfile);
					}else{
						CreateCustomerAddress(address,customerProfile);
					}	
				}
			}
		}
	}
};
module.exports.sendNotification = function (errorMsg,customerProfile){
	var Email = require(CommonConstants.REQUIRE_EMAIL);
    var Resource = require('dw/web/Resource');
    var Site = require(CommonConstants.REQUIRE_SITE);
    var customerNo = customerProfile.customerNo;
    var status= Email.sendMail({
        recipient: Site.getCurrent().getCustomPreferenceValue('smAdminEmail'),
        template: 'mail/notification',
        subject: Resource.msgf('sessionm.mail.servicedown', 'sessionm', null,errorMsg,customerNo),
        context: {
            ErrorMsg: errorMsg,
            CustomerNo : customerNo
        }
    });
    return status;
};
/*
*param sessionM customer profile response
*param customer profile
*returns set customer profile profile in DW
*/
setSMCustomerProfileAttribute = function (smUser,customerProfile){
 var profileArr = CommonConstants.PROFILEARRAY;
	Transaction.wrap(function(){
  		for(var i=0, len=profileArr.length; i<len; i++) {
  			var attribute = profileArr[i];
  			if(attribute in smUser.user_profile){
				customerProfile[profileArr[i]] = smUser.user_profile[profileArr[i]];
  			}
		}
	});
};
/*
*param sessionM response
*param SFCC customer profile
*returns update SFCC customer profile with SM custom attributes
*/
module.exports.updateSMCustomerProfileFields = function (customerProfile,smUser){
	if(null !== smUser){
		var CommonConstants = require('../common/CommonConstants').CommonConstants;
 		var sfccCustomMap = CommonConstants.SFCC_CUSTOM_ATTRIBUTESMAP;
 		Transaction.wrap(function(){
	       			customerProfile.custom.IsSMProfileCreationFailed = false;
			       	for each(var entry in sfccCustomMap ){
			       		if(entry.value in smUser){
			       		   customerProfile.custom[entry.key] = smUser[entry.value];
			       	}
			  }
		});
	}
};
/*
*param sessionM address
*param customer address fetched from sessionM addressID
*returns set customer profile in DW
*/
setSMCustomerAddressAttribute = function (address,customerAddress,customerProfile){
 var addressArr = CommonConstants.ADDRESSARRAY;
	Transaction.wrap(function(){
  		for(var i=0, len=addressArr.length; i<len; i++) {
  			var attribute = addressArr[i];
  			if(attribute in address){
  				customerAddress[addressArr[i]] = address[addressArr[i]];
  			}
  		}
  		if(CommonConstants.USERADDRESS_PREFERRED in address && address.preferred){
  			customerProfile.addressBook.setPreferredAddress(customerAddress);
		}
	});
};
/*
*param sessionM address
*param customer address fetched from sessionM addressID
*returns create new customer address in DW
*/
CreateCustomerAddress = function (address,customerProfile){
 	var addressArr = CommonConstants.ADDRESSARRAY;
  	var addressBook = customerProfile.addressBook;
	var newAddress = null;
	Transaction.wrap(function(){
	if('ID' in address){
		newAddress = addressBook.createAddress(address.ID);
		 for(var i=0, len=addressArr.length; i<len; i++) {
		  	var attribute = addressArr[i];
		  	if(attribute in address){
					newAddress[addressArr[i]] = address[addressArr[i]];
		  	}
		  	if(CommonConstants.USERADDRESS_PREFERRED in address && address.preferred){
		  		customerProfile.addressBook.setPreferredAddress(newAddress);
			}
		}
	}
	});
};

module.exports.getRedemptionReqObj = function (order){
	var adjustment = order.getPriceAdjustmentByPromotionID('rewards_points'),
		amountValue = dw.system.Site.getCurrent().getCustomPreferenceValue('smPointValue'),
		redemedQuanity = -1 * adjustment.basePrice.value * 100;
	var orderInfo = {
						'order': {
							'quantity': redemedQuanity,
							'ip': order.remoteHost
			  }
			};
	var	requestObj = {
						orderInfo : JSON.stringify(orderInfo),
						customerNo : customer.profile.getCustomerNo()
					};
	return requestObj;
};

module.exports.handleRedemptionResponse = function (response , order){
	var responseObj = JSON.parse(response.object);
	if(CommonConstants.SERVICE_SUCCESS.equalsIgnoreCase( responseObj.status)){
		require('dw/system/Transaction').wrap(function(){
			var adjustment = order.getPriceAdjustmentByPromotionID('rewards_points'),
			amountValue = dw.system.Site.getCurrent().getCustomPreferenceValue('smPointValue'),
			redemedPoints = -1 * adjustment.basePrice.value / amountValue;
	 		customer.profile.custom.smAvailable_points = responseObj.order.user.available_points;
	 		order.custom.smOrderTobeRedeemed = false;
	 		order.custom.smOrderCustom1 = redemedPoints;
	 		order.custom.smOrderCustom2 = responseObj.order.id;
 		});
 		return true;
	} else {
		return false;
	}
};

updateOnRedemptionFailure = function ( order ){
	require('dw/system/Transaction').wrap(function(){
		var adjustment = order.getPriceAdjustmentByPromotionID('rewards_points'),
			amountValue = dw.system.Site.getCurrent().getCustomPreferenceValue('smPointValue'),
			redemedPoints = -1 * adjustment.basePrice.value / amountValue;
 		customer.profile.custom['smAvailable_points'] = customer.profile.custom['smAvailable_points'] - redemedPoints;
 		order.custom.smOrderCustom1 = redemedPoints;
	});
};
/*
*param customerProfile
*returns current tier details for customer
*/
module.exports.UpdateTierDetails= function (customerProfile) {
	var CustomObjectMgr = require('dw/object/CustomObjectMgr');
	var tierCOIterator=CustomObjectMgr.getAllCustomObjects('tierData');
	var availablePoints = customerProfile.custom.smAvailable_points;
	var currentTier;
	if(tierCOIterator.getCount() > 0) {
		var sortedList = [];
		while(tierCOIterator.hasNext()) {
			var tierdata = tierCOIterator.next();
			sortedList.push(tierdata);
		}
		sortedList.sort( function(e1,e2){
		var point1 = new Number(e1.custom.smRequiredPoints);
		var point2 = new Number(e2.custom.smRequiredPoints);
			if ( point1 > point2 ) {
				return 1;
			} if ( point1 < point2 ) {
				return -1;
			} else {
				return -1;
			}
		});
		currentTier = calculateSMTier(sortedList, availablePoints);
		if(null !== currentTier){
			Transaction.wrap(function(){
				customerProfile.custom.smTierName = currentTier.currentTierName;
			});
		}
	}
	return currentTier;
};
/*
*param sortedTierList
**param customer availablePoints
*returns current tier details for customer
*/
calculateSMTier= function (sortedTierList, availablePoints) {
	var currentTier = {};
	if (sortedTierList.length > 0){
		switch(true){
		case  sortedTierList[0].custom.smRequiredPoints > availablePoints :
			currentTier.requiredPoint =  sortedTierList[0].custom.smRequiredPoints;
			currentTier.requiredPointForNextTier = currentTier.requiredPoint-availablePoints;
			currentTier.nextTierName = sortedTierList[0].custom.smName;
			currentTier.currentTierName = null;
		      break;
		case sortedTierList[sortedTierList.length-1].custom.smRequiredPoints <= availablePoints:
			currentTier.currentTierName = sortedTierList[sortedTierList.length-1].custom.smName;
			currentTier.requiredPointForNextTier = null;
			currentTier.nextTierName = null;
			  break;
		default :                 
			for (var i=0; i < sortedTierList.length; i++){
			   if (sortedTierList[i].custom.smRequiredPoints > availablePoints ){
				   currentTier.requiredPoint =  sortedTierList[i].custom.smRequiredPoints;
				   currentTier.requiredPointForNextTier = currentTier.requiredPoint-availablePoints;
				   currentTier.nextTierName = sortedTierList[i].custom.smName;
				   currentTier.currentTierName = sortedTierList[i-1].custom.smName;
			   break;
			   }else if (sortedTierList[i].custom.smRequiredPoints === availablePoints ){
				   currentTier.requiredPoint =  sortedTierList[i].custom.smRequiredPoints;
				   currentTier.requiredPointForNextTier = sortedTierList[i+1].custom.smRequiredPoints-availablePoints;
				   currentTier.currentTierName = sortedTierList[i].custom.smName;
				   currentTier.nextTierName = sortedTierList[i+1].custom.smName;
				   break;
			   }
			}
		}
	}
	return currentTier;
}

/**
 * Copy source file to target location. All directories will be created if is necessary.
 *
 */
module.exports.copyFile = function(file, archiveFolder, sourceFolder){
	var File = require('dw/io/File');
	var FileReader = require('dw/io/FileReader');
	var FileWriter = require('dw/io/FileWriter');
	var target = new File(archiveFolder.fullPath + File.SEPARATOR + file.name);
			var fileReader = new FileReader(file);
			var fileWriter = new FileWriter(target);
			var bytesToCopy = file.length();
			var buffer;
			do{
				if (bytesToCopy > 10240) {			
					buffer = fileReader.read(10240);
					bytesToCopy = bytesToCopy - 10240;
				} else {
					buffer = fileReader.read(bytesToCopy);
					bytesToCopy = 0;
				}
				if(buffer != null){
					fileWriter.write(buffer);
				}
			} while(bytesToCopy != 0);
			
			fileReader.close();
			fileWriter.flush();
			fileWriter.close();
			var deleteFile = new File(File.IMPEX + sourceFolder + File.SEPARATOR + file.name);
			var success = deleteFile.remove();
			if(success){
				return {'done' : true, 'message' : 'File archived and deleted successfully'};
			}else{
				return {'done' : false, 'message' : 'Error while archiving and deleting file'};
			}
	}



/*
*param sessionM response
*param SFCC customer profile
*returns update SFCC customer rewards point with SM custom attributes
*/
module.exports.updateCustomerRewardPoints = function (customerProfile,smUser){
	if(null !== smUser){
 		Transaction.wrap(function(){
	       			customerProfile.custom.smAvailable_points = smUser.available_points;
	       			customerProfile.custom.IsSMProfileSyncFailed = false;
		});
	}
};
module.exports.updateOnRedemptionFailure = updateOnRedemptionFailure;