/* Copyright (C) 2017 SessionM. All Rights Reserved*/
/* 
* ServiceInit.ds will contains service definitions
*/
var system  = require('dw/system');
var net  = require('dw/net');
var util  = require('dw/util');
var svcConfig  = require('dw/svc');
var CommonConstants = require('~/cartridge/scripts/common/CommonConstants').CommonConstants;
/*********************************************************************************
* Service Name : int_sessionm_createprofile
* Input 	   : request object holds the customer profile Object
*
/*********************************************************************************/

svcConfig.ServiceRegistry.configure(CommonConstants.CREATE_PROFILE_SERVICE_ID,{
/**
* Description  : Method to Create request for int_sessionm_createprofile service
* Input 	   : requestObj Object
* output	   : service Request
*
/**/
	createRequest: function(svc, requestObject){
			var bytedata = svc.getConfiguration().getCredential().user+':'+svc.getConfiguration().getCredential().getPassword();
			var encodedAuth  = dw.crypto.Encoding.toBase64(new dw.util.Bytes(bytedata));
			svc.addHeader(CommonConstants.AUTHORIZATION,CommonConstants.BASIC+ encodedAuth);
			svc.addHeader(CommonConstants.CONTENT_TYPE_HEADER, CommonConstants.APPLICATION_JSON);
		return requestObject;
	},
/**
* Description  : Method to get the response from int_sessionm_createprofile service
* Input 	   : response object
* output	   : service response
/**/
	parseResponse: function(svc, client) {
		return client.text;
	},
/**
* Description  : Method to Create Mock request for int_sessionm_createprofile service
* Input 	   : Customer Object
* output	   : service Request
/**/
	mockCall:function(){
		return {
	        		statusCode: 500,
        			statusMessage: 'ERROR',
        			text:null
			};
},
/**
* Description  : Method to filter the logging of request for int_sessionm_createprofile service
* Input 	   : string
* output	   : request
/**/
	filterLogMessage: function(msg) {
		return msg;
	},
	getResponseLogMessage: function(response) {
		return response.text;
	},
	getRequestLogMessage: function(request) {
		return request;
	},
});
/**************************************************************************************
* Service Name : int_sessionm_updateprofile
* Input 	   : request object holds the customer profile Object with values to update
*
/**************************************************************************************/
/*
* Description  : Method to Initialize int_sessionm_updateprofile service
* Input 	   : None
* output	   : Service Client
*
/***/

svcConfig.ServiceRegistry.configure(CommonConstants.UPDATE_PROFILE_SERVICE_ID,{
/**
* Description  : Method to Create request for int_sessionm_updateprofile service
* Input 	   : requestObj Object
* output	   : service Request
*
/**/

	createRequest: function(svc, requestObject){
			var bytedata = svc.getConfiguration().getCredential().user+':'+svc.getConfiguration().getCredential().getPassword();
			var encodedAuth = dw.crypto.Encoding.toBase64(new dw.util.Bytes(bytedata));
			svc.addHeader(CommonConstants.AUTHORIZATION,CommonConstants.BASIC+ encodedAuth);
			svc.addHeader(CommonConstants.CONTENT_TYPE_HEADER, CommonConstants.APPLICATION_JSON);
			svc.setRequestMethod(CommonConstants.PUT);
			var url  = svc.getURL();
			url = url.replace(/smuuid/g, customer.profile.custom.smUUID);
			svc.setURL(url);
		return requestObject;
	},
/**
* Description  : Method to get the response from int_sessionm_updateprofile service
* Input 	   : HTTP client
* output	   : service response
*
/**/
	parseResponse: function(svc, client) {
		return client.text;
	},
/**
* Description  : Method to Create Mock request for int_sessionm_updateprofile service
* Input 	   : Customer Object
* output	   : service Request
*
/**/
	mockCall:function(){
		return {
	        		statusCode: 500,
        			statusMessage: 'ERROR',
        			text:null
			};
	},
/**
* Description  : Method to filter the logging of request for int_sessionm_updateprofile service
* Input 	   : string
* output	   : request
*
/**/
	filterLogMessage: function(msg) {
		return msg;
	},
	getRequestLogMessage: function(request) : String {
		return request;
	},
	getResponseLogMessage: function(response) : String {
		return response.text;
	}
});


/**************************************************************************************
* Service Name : int_sessionm_getprofile
* Input 	   : external user ID to be sent in parameter in URL
*
/**************************************************************************************/


/*
* Description  : Method to Initialize int_sessionm_getprofile service
* Input 	   : None
* output	   : Service Client
*
/***/
svcConfig.ServiceRegistry.configure(CommonConstants.GET_PRFL_SRV_ID,{
/**
* Description  : Method to Create request for int_sessionm_getprofile service
* Input 	   : requestObj
* output	   : service Request
/**/
	 createRequest: function(svc, args){
	 	var bytedata = svc.getConfiguration().getCredential().user+':'+svc.getConfiguration().getCredential().getPassword();
			var encodedAuth = dw.crypto.Encoding.toBase64(new dw.util.Bytes(bytedata));
			svc.addHeader(CommonConstants.AUTHORIZATION,CommonConstants.BASIC + encodedAuth);
			svc.addHeader(CommonConstants.CONTENT_TYPE_HEADER, CommonConstants.APPLICATION_JSON);
			svc.setRequestMethod(CommonConstants.GET);
		var url = svc.getURL();
			url = url.replace(/{external_id}/, args);
		 	svc.setURL(url);
       },
/**
* Description  : Method to get the response from int_sessionm_getprofile service
* Input 	   : HTTP client
* output	   : service response
/**/
	parseResponse: function(svc, client) {
		return client.text;
	},
/**
* Description  : Method to Create Mock request for int_sessionm_getprofile service
* Input 	   : Customer Object
* output	   : service Request
/**/
	mockCall:function(){
		return {
	        		statusCode: 500,
        			statusMessage: 'ERROR',
        			text:null
			};
	},
/**
* Description  : Method to filter the logging of request for int_sessionm_getprofile service
* Input 	   : string
* output	   : request
/**/
	filterLogMessage: function(msg) {
		return msg;
	},
	getRequestLogMessage: function(request ) : String {
		return request;
	},
	getResponseLogMessage: function(response ) : String {
		return response.text;
	}
});
/**************************************************************************************
* Service Name : int_sessionm_getprofile
* Input 	   : external user ID to be sent in parameter in URL
*
/**************************************************************************************/


/*
* Description  : Method to Initialize int_sessionm_getprofile service
* Input 	   : None
* output	   : Service Client
*
/***/
svcConfig.ServiceRegistry.configure(CommonConstants.SRH_PRFL_SRV_ID,{
/**
* Description  : Method to Create request for int_sessionm_getprofile service
* Input 	   : requestObj
* output	   : service Request
/**/
	 createRequest: function(svc, args){
        var bytedata = svc.getConfiguration().getCredential().user+':'+svc.getConfiguration().getCredential().getPassword();
		var encodedAuth = dw.crypto.Encoding.toBase64(new dw.util.Bytes(bytedata));
			svc.addHeader(CommonConstants.AUTHORIZATION,CommonConstants.BASIC + encodedAuth);
			svc.addHeader(CommonConstants.CONTENT_TYPE_HEADER, CommonConstants.APPLICATION_JSON);
			svc.setRequestMethod(CommonConstants.GET);
		var url = svc.getURL();
			url = url.replace(/{email}/, args);
		 	svc.setURL(url);
       },
/**
* Description  : Method to get the response from int_sessionm_getprofile service
* Input 	   : HTTP client
* output	   : service response
/**/
	parseResponse: function(svc, client) {
		return client.text;
	},
/**
* Description  : Method to Create Mock request for int_sessionm_getprofile service
* Input 	   : Customer Object
* output	   : service Request
/**/
	mockCall:function(){
		return {
	        		statusCode: 500,
        			statusMessage: 'ERROR',
        			text:null
			};
	},
/**
* Description  : Method to filter the logging of request for int_sessionm_getprofile service
* Input 	   : string
* output	   : request
/**/
	filterLogMessage: function(msg) {
		return msg;
	},
	getRequestLogMessage: function(request ) : String {
		return request;
	},
	getResponseLogMessage: function(response ) : String {
		return response.text;
	}
});
/**************************************************************************************
* Service Name : int_sessionm_retrievemultipleprofile
* Input 	   : Array of smUUId's
*
/**************************************************************************************/


/*
* Description  : Method to Initialize int_sessionm_retrievemultipleprofile service
* Input 	   : None
* output	   : Service Client
*
/***/
svcConfig.ServiceRegistry.configure(CommonConstants.FTH_MULTI_PRFL_SRV_ID,{
/**
* Description  : Method to Create request for int_sessionm_retrievemultipleprofile service
* Input 	   : requestObj
* output	   : service Request
/**/
	 createRequest: function(svc, args){
	 	var bytedata = svc.getConfiguration().getCredential().user+':'+svc.getConfiguration().getCredential().getPassword();
			var encodedAuth = dw.crypto.Encoding.toBase64(new dw.util.Bytes(bytedata));
			svc.addHeader(CommonConstants.AUTHORIZATION,CommonConstants.BASIC + encodedAuth);
			svc.addHeader(CommonConstants.CONTENT_TYPE_HEADER, CommonConstants.APPLICATION_JSON);
			svc.setRequestMethod(CommonConstants.GET);
		var url = svc.getURL();
			url = url.replace(/{external_ids}/, args);
		 	svc.setURL(url);
       },
/**
* Description  : Method to get the response from int_sessionm_getprofile service
* Input 	   : HTTP client
* output	   : service response
/**/
	parseResponse: function(svc, client) {
		return client.text;
	},
/**
* Description  : Method to Create Mock request for int_sessionm_getprofile service
* Input 	   : Customer Object
* output	   : service Request
/**/
	mockCall:function(){
		return {
	        		statusCode: 500,
        			statusMessage: 'ERROR',
        			text:null
			};
	},
/**
* Description  : Method to filter the logging of request for int_sessionm_getprofile service
* Input 	   : string
* output	   : request
/**/
	filterLogMessage: function(msg) {
		return msg;
	},
	getRequestLogMessage: function(request ) : String {
		return request;
	},
	getResponseLogMessage: function(response ) : String {
		return response.text;
	}
});
/*
* Description  : Method to Initialize FETCH_TIER_DETAIL_SERVICE_ID service
* Input 	   : None
* output	   : Service Client
*
/***/
svcConfig.ServiceRegistry.configure(CommonConstants.TIER_SRV_ID,{
/**
* Description  : Method to Create request for int_sessionm_retrievemultipleprofile service
* Input 	   : requestObj
* output	   : service Request
/**/
	 createRequest: function(svc, args){
	 	var bytedata = svc.getConfiguration().getCredential().user+':'+svc.getConfiguration().getCredential().getPassword();
			var encodedAuth = dw.crypto.Encoding.toBase64(new dw.util.Bytes(bytedata));
			svc.addHeader(CommonConstants.AUTHORIZATION,CommonConstants.BASIC + encodedAuth);
			svc.addHeader(CommonConstants.CONTENT_TYPE_HEADER, CommonConstants.APPLICATION_JSON);
			svc.setRequestMethod(CommonConstants.GET);
       },
/**
* Description  : Method to get the response from int_sessionm_getprofile service
* Input 	   : HTTP client
* output	   : service response
/**/
	parseResponse: function(svc, client) {
		return client.text;
	},
/**
* Description  : Method to Create Mock request for int_sessionm_getprofile service
* Input 	   : Customer Object
* output	   : service Request
/**/
	mockCall:function(){
		return {
	        		statusCode: 200,
        			statusMessage: 'OK',
        			text:'{"status":"ok","tiers":[{"id":11,"name":"Platinum","identifier":"platinum","tier_system_id":5},'+
        			'{"id":12,"name":"Gold","identifier":"gold","next_tier_id":11,"required_sponsor_points":2000,"tier_system_id":5},'+
        			'{"id":13,"name":"Silver","identifier":"silver","next_tier_id":12,"required_sponsor_points":1000,"tier_system_id":5}]}'
			};
	},
/**
* Description  : Method to filter the logging of request for int_sessionm_getprofile service
* Input 	   : string
* output	   : request
/**/
	filterLogMessage: function(msg) {
		return msg;
	},
	getRequestLogMessage: function(request ) : String {
		return request;
	},
	getResponseLogMessage: function(response ) : String {
		return response.text;
	}
});
svcConfig.ServiceRegistry.configure(CommonConstants.EXPORT_ORDER_SERVICE_ID,{
	/**
	* Description  : Method to Create request for int_sessionm_exportorder service
	* Input 	   : requestObj Object
	* output	   : service Request
	*
	/**/
		createRequest: function(svc, requestObject){
				var bytedata = svc.getConfiguration().getCredential().user+':'+svc.getConfiguration().getCredential().getPassword();
				var encodedAuth  = dw.crypto.Encoding.toBase64(new dw.util.Bytes(bytedata));
				svc.addHeader(CommonConstants.AUTHORIZATION,CommonConstants.BASIC + encodedAuth);
			return requestObject;
		},
	/**
	* Description  : Method to get the response from int_sessionm_exportorder service
	* Input 	   : response object
	* output	   : service response
	/**/
		parseResponse: function(svc, client) {
			return client.text;
		},
	/**
	* Description  : Method to Create Mock request for int_sessionm_exportorder service
	* Input 	   : Customer Object
	* output	   : service Request
	/**/
		mockCall:function(){
	},
	/**
	* Description  : Method to filter the logging of request for int_sessionm_exportorder service
	* Input 	   : string
	* output	   : request
	/**/
		filterLogMessage: function(msg) {
			return msg;
		},
		getRequestLogMessage: function(request ) : String {
			return request;
		},
		getResponseLogMessage: function(response ) : String {
			return response.text;
		}
	});
	svcConfig.ServiceRegistry.configure('int_sessionm_sftpservice',{
	/**
	* Description  : Method to Create request for int_sessionm_sftpservice service
	* Input 	   : requestObj Object
	* output	   : service Request
	*
	/**/
		createRequest: function(svc, requestObject){
			return requestObject;
		},
	/**
	* Description  : Method to get the response from int_sessionm_exportorder service
	* Input 	   : response object
	* output	   : service response
	/**/
		parseResponse: function(svc, result) {
			return result;
		},
	/**
	* Description  : Method to Create Mock request for int_sessionm_exportorder service
	* Input 	   : Customer Object
	* output	   : service Request
	/**/
		mockCall:function(){
	},	
	/**
	* Description  : Method to filter the logging of request for int_sessionm_exportorder service
	* Input 	   : string
	* output	   : request
	/**/
		filterLogMessage: function(msg) {
			return msg;
		},
		getRequestLogMessage: function(request ) : String {
			return request;
		},
		getResponseLogMessage: function(response ) : String {
			return response;
		}
	});
/*
* Description  : Method to Initialize int_sessionm_order service
* Input 	   : None
* output	   : Service Client
*
/***/
svcConfig.ServiceRegistry.configure('int_sessionm_order',{
/**
* Description  : Method to Create request for int_sessionm_order service
* Input 	   : requestObj
* output	   : service Request
/**/
	 createRequest: function(svc, args){
	 	var bytedata = svc.getConfiguration().getCredential().user+':'+svc.getConfiguration().getCredential().getPassword();
		var encodedAuth = dw.crypto.Encoding.toBase64(new dw.util.Bytes(bytedata));
		svc.addHeader('Authorization','BASIC '+ encodedAuth);
		svc.addHeader('Content-Type', 'application/json');
		svc.setRequestMethod( 'POST' );
		var url  = svc.getURL();
		url = url.replace(/external_id/g, args.customerNo );
		svc.setURL(url);
		return args.orderInfo;
       },
/**
* Description  : Method to get the response from int_sessionm_order service
* Input 	   : HTTP client
* output	   : service response
/**/
	parseResponse: function(svc, client) {
		return client.text;
	},
	mockCall:function(){
		var response = '{"status":"ok","order":{"id":1119,"user_id":"ef9ef0ac-1945-11e7-9894-3cd763215364","offer_id":42,"quantity":1000,"points":10000,"status":"pending_approval","created_at":"2017-05-03T12:57:50Z","name":"Points as Cash","logo":"https://content-stg-sessionm-com.s3.amazonaws.com/1/67/d/1827/Test_2.jpg","description":null,"offer_type":"balance transfer","user":{"id":"ef9ef0ac-1945-11e7-9894-3cd763215364","available_points":83000,"test_points":0}}}';
		return {
	        		statusCode: 200,
        			statusMessage: 'OK',
        			text:response
			};
	},
/**
* Description  : Method to filter the logging of request for int_sessionm_order service
* Input 	   : string
* output	   : request
/**/
	filterLogMessage: function(msg) {
		return msg;
	},
	getRequestLogMessage: function(request ) : String {
		return request;
	},
	getResponseLogMessage: function(response ) : String {
		return response.text;
	}
});