/* Copyright (C) 2017 SessionM. All Rights Reserved */

/*
* Perform service call for Redeeming SessionM points
* param - order
*/
var ComConst = require('../common/CommonConstants').CommonConstants;  
var helper = require(ComConst.PATH_HELPER+'SMHelper');
var Logger = require(ComConst.REQUIRE_LOGGER);

function redeemSessionmPoints(order){
 var result= false;
 try{
		var svc = require(ComConst.DW_SVC_PATH);
		var service = svc.ServiceRegistry.get('int_sessionm_order');
		var requestObj = helper.getRedemptionReqObj( order );
		var	 response = service.call(requestObj);
		if ((!empty(response)) &&
	 		('OK'.equals( response.getStatus()))){
			result=helper.handleRedemptionResponse( response , order );
	 	} else{
	 		helper.updateOnRedemptionFailure( order );
	 		result = true;
	 	}
	}catch(e){
			Logger.error('redeemSessionmPoints method :  Error in exporting redemption order ( {0} )',e.message);
			return result;
	}finally{
		return result;
	}
}
/*
* Initiate multiple customer fetch in SessionM
* param - customer uuid array
* @returns SM retrieve multiple profile service response
*/
function fetchSMTierDetails(){
	var serviceResult = {};
	var responseData = {} ;
	try{
		var svc = require(ComConst.DW_SVC_PATH);
		var  service = svc.ServiceRegistry.get(ComConst.TIER_SRV_ID);
        var	 response = service.call();
        //Validate the WebService response
        if ((!empty(response)) &&
        	 ('OK'.equals( response.getStatus()))){
        		responseData = JSON.parse(response.object);
        	 }
		//setting request and response for this call
		serviceResult.responseObj = responseData;
	}catch(e){
			Logger.error('fetchSMTierDetails :  Error occured while retrieving  SM tier details ( {0} )',e.message);
	}finally{
		return serviceResult;
	}
}

module.exports = {
		'RedeemSessionmPoints' : redeemSessionmPoints,
		'FetchSMTierDetails' : fetchSMTierDetails
};