/* Copyright (C) 2017 SessionM. All Rights Reserved */

/*
* Facade methods for Customer profile
* param - order
*/
var svc : Object = require('dw/svc');
var ComConst = require('../common/CommonConstants').CommonConstants;
var helper = require(ComConst.PATH_HELPER+'SMHelper');
var Logger = require(ComConst.REQUIRE_LOGGER);

function createCustomer(customerProfile){
	var serviceResult  = {};
	var response;
	try{
		// Load the service configuration
		var service  =  svc.ServiceRegistry.get(ComConst.CREATE_PROFILE_SERVICE_ID);
		//creates request for Create Profile from Customer DTO
		var requestObj = helper.customerProfileReqObject( customerProfile );
		//Call the SM for Customer Profile creation
		var Site = require(ComConst.REQUIRE_SITE);
		var retryAttempt = Site.getCurrent().getCustomPreferenceValue('SMRetryAttempt');
		for(i=0; i<retryAttempt;i++){
			response = service.call(requestObj);
			var parsedResponse = JSON.parse(response.object);
			if(!empty(response.object) && 'ok'.equalsIgnoreCase(parsedResponse.status)){
				break;
			}
		}
		if(!empty(response) && 'ERROR'.equalsIgnoreCase(response.status)){
		 	helper.sendNotification(ComConst.CREATEPROFILE_NOTWORKING,customerProfile);
		 	require(ComConst.DW_TRANSACTION_PATH).wrap(function(){
		 		customerProfile.custom.IsSMProfileCreationFailed = true;
		 	});
		}
		//Validate the WebService response
		var responseObj = helper.customerProfileResponseObject(response);
		//setting request and response for this call
		serviceResult.requestObj = requestObj;
		serviceResult.responseObj = responseObj;
	}
	catch(e){
		Logger.error('Error occured while creating customer profile '+ e);
	}
	finally{
		return serviceResult;
	}
};


function updateCustomer(customerProfile){
	var serviceResult = {};
	try{
		// Load the service configuration
		var service =  svc.ServiceRegistry.get(ComConst.UPDATE_PROFILE_SERVICE_ID);
		//creates request for update Profile from Address DTO
		var requestObj = helper.customerProfileReqObject( customerProfile );
		var response;
		//Call the Update Customer WebService
		var Site = require(ComConst.REQUIRE_SITE);
		var retryAttempt = Site.getCurrent().getCustomPreferenceValue('SMRetryAttempt');
		for(i=0; i<retryAttempt;i++){
			response = service.call(requestObj);
			var parsedResponse = JSON.parse(response.object);
			if(!empty(response.object) && 'ok'.equalsIgnoreCase(parsedResponse.status)){
				break;
			}
		}
		if(!empty(response) && 'ERROR'.equalsIgnoreCase(response.status)){
		 	helper.sendNotification(ComConst.UPDATEPROFILE_NOTWORKING,customerProfile);
		}
		//Validate the WebService response
		var responseObj = helper.customerProfileResponseObject( response );
		//setting request and response for this call
		serviceResult.requestObj = requestObj;
		serviceResult.responseObj = responseObj;
	}
	catch(e){
		Logger.error('Update Customer : Error occured while updating customer profile '+ e);
	}
	finally{
		return serviceResult;
	}
};
/*
* Initiate the customersearch in SessionM
* param - customer profile
* returns SM search service response
*/
function searchCustomer(customerProfile){
	var serviceResult = {};
	try{
			 var customerEmail = customerProfile.email;
			 var service = svc.ServiceRegistry.get(ComConst.SRH_PRFL_SRV_ID);
        	 var response = service.call(customerEmail);
        	 var parsedResponse = JSON.parse(response.object);
        	 /*update flag count in case of error or service failure*/
        	if(!empty(response) && 'ERROR'.equalsIgnoreCase(response.status)
        	 	|| 'error'.equalsIgnoreCase(parsedResponse.status) || null === parsedResponse){
		 		require(ComConst.DW_TRANSACTION_PATH).wrap(function(){
		 		var searchCount = customerProfile.custom.smSearchFailCount;
		 		customerProfile.custom.smSearchFailCount = searchCount + 1;
		 		});
			 }
        	 //Validate the WebService response
			 var responseObj = helper.customerProfileResponseObject( response );
			 //setting request and response for this call
			 serviceResult.responseObj = responseObj;
	}catch(e){
			Logger.error('Search customer:  Error occured while searching customer Profile ( {0} )',e.message);
	}finally{
		return serviceResult;
	}
}
/*
* Initiate customer fetch in SessionM
* param - customer profile
* returns SM retrieve service response
*/
function retrieveCustomer(customerProfile){
	var serviceResult = {};
	try{
		var ComConst = require('../common/CommonConstants').CommonConstants;
		var  smExternalId = customerProfile.customerNo;
		var  service = svc.ServiceRegistry.get(ComConst.GET_PRFL_SRV_ID);
        var	 response ;
        var Site = require(ComConst.REQUIRE_SITE);
        /*Call service for attempts defined in BM*/
        var retryAttempt = Site.getCurrent().getCustomPreferenceValue('SMRetryAttempt');
		for(i=0; i<retryAttempt;i++){
			response = service.call(smExternalId);
			var parsedResponse = JSON.parse(response.object);
			if(!empty(response.object) && 'ok'.equalsIgnoreCase(parsedResponse.status)){
				break;
			}
		}
        if(!empty(response) && 'ERROR'.equalsIgnoreCase(response.status)){
		 		require(ComConst.DW_TRANSACTION_PATH).wrap(function(){
		 		customerProfile.custom.IsSMProfileSyncFailed = true;
		 	});
		}
        //Validate the WebService response
		var responseObj = helper.customerProfileResponseObject( response );
		//setting request and response for this call
		serviceResult.responseObj = responseObj;
	}catch(e){
			Logger.error('RetrieveCustomer :  Error occured while retrieving customer Profile ( {0} )',e.message);
	}finally{
		return serviceResult;
	}
}
/*
* Initiate multiple customer fetch in SessionM
* param - customer uuid array
* returns SM retrieve multiple profile service response
*/
function retrieveMultipleProfile(external_ids){
	var serviceResult = {};
	try{
		var  service = svc.ServiceRegistry.get(ComConst.FTH_MULTI_PRFL_SRV_ID);
        var	 response = service.call(external_ids);
        //Validate the WebService response
		var responseObj = helper.customerProfileResponseObject( response );
		//setting request and response for this call
		serviceResult.responseObj = responseObj;
	}catch(e){
			Logger.error('RetrieveMultipleProfile :  Error occured while retrieving  multiple customer Profile ( {0} )',e.message);
	}finally{
		return serviceResult;
	}
}

/*
* Initiate the service to get customers profile to update 
* customer reward points
* returns SM retrieve profile service response
*/
function getCustomerRewardPoints(customerProfile){
	var serviceResult = {};
	try{
		var  smExternalId = customerProfile.customerNo;
		var  service = svc.ServiceRegistry.get(ComConst.GET_PRFL_SRV_ID);
        var	 response ;
        var Site = require(ComConst.REQUIRE_SITE);
        /*Call service for attempts defined in BM*/
        var retryAttempt = Site.getCurrent().getCustomPreferenceValue('SMRetryAttempt');
		for(i=0; i<retryAttempt;i++){
			response = service.call(smExternalId);
			var parsedResponse = JSON.parse(response.object);
			if(!empty(response.object) && 'ok'.equalsIgnoreCase(parsedResponse.status)){
				break;
			}
		}
        if(!empty(response) && 'ERROR'.equalsIgnoreCase(response.status)){
		 		require(ComConst.DW_TRANSACTION_PATH).wrap(function(){
		 		customerProfile.custom.IsSMProfileSyncFailed = true;
		 	});
		}
        //Validate the WebService response
		var responseObj = helper.customerProfileResponseObject( response );
		//setting request and response for this call
		serviceResult.responseObj = responseObj;
	}catch(e){
			Logger.error('Retrieve Customer Reward Points :  Error occured while retrieving customer Profile ( {0} )',e.message);
	}finally{
		return serviceResult;
	}
};
module.exports = {
	'UpdateCustomer': updateCustomer,
	'CreateCustomer': createCustomer,
	'RetrieveCustomer' :retrieveCustomer,
	'SearchCustomer' :searchCustomer,
	'RetrieveMultipleProfile' :retrieveMultipleProfile
};