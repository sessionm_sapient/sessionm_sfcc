/* Copyright (C) 2017 SessionM. All Rights Reserved*/

/* 
* SessionM.js will contains all the controllers for SessionM loyalty management
*/

var ComConst = require('int_sessionm_core/cartridge/scripts/common/CommonConstants').CommonConstants;
var guard = require(ComConst.GUARD);
var app = require(ComConst.APP);
/**
* @module controllers/SessionM
*/

/**
 *  Hook to initiate the customer registration with SessionM
 */
function registerCustomer(){
   var Adaptor = require(ComConst.PATH_ADAPTOR+'CustomerProfile');
   var result = Adaptor.Registration(customer.profile);
   return result;
}

/**
 *  Hook to initiate the customer registration with SessionM
 */
function retrieveCustomer(){
	 var CustAdaptor = require(ComConst.PATH_ADAPTOR+'CustomerProfile');
	 var result = CustAdaptor.RetrieveCustomer(customer.profile);
	 return result;
}

/**
 * Hook to update the customer profile with SessionM
 */

function updateCustomer(){
	 var CustAdaptor = require(ComConst.PATH_ADAPTOR+'CustomerProfile');
	 var result = CustAdaptor.UpdateCustomer(customer.profile);
	 return result;
}

/**
 * Controller to show Rewards Page
 */

function showRewards(){
		app.getView().render('rewards/rewards');
}

/**
 * Controller to redeem rewards points
 */
function redeemPoints(){
	var redeemableAmount,cart;
	redeemableAmount= request.httpParameterMap.redeemableAmount.stringValue;
	cart = app.getModel('Cart').get();
	var RewardsAdaptor = require(ComConst.PATH_ADAPTOR+'Rewards');
	var result = RewardsAdaptor.CalculateReward(cart.object,redeemableAmount);
	if(result.success){
		 let r = require(ComConst.SG_CONTROLLER+'/cartridge/scripts/util/Response');
	        r.renderJSON({
	            success: true
	        });
	}
 return result;
}

/**
 * Controller to remove rewards points promotion
 */
function removeRewardspromo(){
	cart = app.getModel('Cart').get();
	var RewardsAdaptor = require(ComConst.PATH_ADAPTOR+'Rewards');
	var result = RewardsAdaptor.RemoveRewards(cart.object);
	if(result.success){
		 let r = require(ComConst.SG_CONTROLLER+'/cartridge/scripts/util/Response');
	        r.renderJSON({
	            success: true
	        });
	}
	return result;
}

/**
*  Hook to export order for redemption
*/
function redeemOrder(order){
	 var RewardsAdaptor = require(ComConst.PATH_ADAPTOR+'Rewards');
	 var result = RewardsAdaptor.ExportRewardRedemption(order);
	 if(!result){
		return RewardsAdaptor.CancelRedemptionOrder(order);
     } else {
		 return {
			error: false
		};
	 }
}


/**
*  Hook to handle error codes for redemption
*/


function handleErrorCodeForRedemptionResponse(){
	var authToken = request.httpParameterMap.authToken.stringValue;
	var ComConst = require('int_sessionm_core/cartridge/scripts/common/CommonConstants').CommonConstants;
	let r = require(ComConst.SG_CONTROLLER+'/cartridge/scripts/util/Response');
	if(empty(authToken)||!authToken.equals(customer.profile.custom.smAuthToken)){
		r.renderJSON({
	           success :false
	    });
	}
	
	var CustAdaptor = require(ComConst.PATH_ADAPTOR+'/CustomerProfile');
	var result = CustAdaptor.RetrieveCustomer(customer.profile);
    if(result.responseObj.status.equalsIgnoreCase('OK')){
		    r.renderJSON({
	            success :true
	   });
	}else{
		 r.renderJSON({
	            success :false
	        });
	}
}

/**
 *  Hook to initiate customer reward points sync with SessionM
 */
function updateRewardPoints(){
   var Adaptor = require(ComConst.PATH_ADAPTOR+'CustomerProfile');
   var result = Adaptor.UpdateCustomerRewardsPoints(customer.profile);
   return result;
}

exports.UpdateRewardPoints = updateRewardPoints;
exports.RegisterCustomer = registerCustomer;
exports.RetrieveCustomer = retrieveCustomer;
exports.UpdateCustomer = updateCustomer;
exports.RedeemOrder = redeemOrder;
exports.HandleErrorCodeForRedemptionResponse = guard.ensure(['post', 'https', 'loggedIn'], handleErrorCodeForRedemptionResponse);
exports.ShowRewards = guard.ensure(['get', 'https', 'loggedIn'], showRewards);
exports.RedeemRewards = guard.ensure(['get','https','loggedIn'],redeemPoints);
exports.RemoveRewardsPromo = guard.ensure(['get','https','loggedIn'],removeRewardspromo);